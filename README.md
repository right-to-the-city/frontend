# Запуск

- Необходимо установить NodeJS v20.11.0
- Создать в корне .env (смотреть .env.example)

## Установка и запуск в dev режиме

### Устанавливаем зависимости
```
npm install
```

### Запускаем в dev режиме
```
npm run dev
```

## Установка и запуск в prod режиме

### Устанавливаем зависимости
```
npm install
```

### Блидим проект
```
npm run build
```

### Запускаем билд
```
npm run preview
```
