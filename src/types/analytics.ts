export type AnalyticsRating = {
  rating: number;
  count: number;
}
export type AnalyticsCategory = {
  category: number;
  count: number;
}

export type AnalyticsGeography = {
  address: string;
  count: number;
}