export type MapObject = {
  category: string;
  comment: string;
  date: string;
  id: number;
  lat: number;
  lng: number;
  media: string[];
  rating: number;
  tags: string[];
  user: {username: string; type: string};
  user_login: string;
}