import { TypesOrNull } from "./utils";

export type User = TypesOrNull<{
	id: number;
	username: string | null;
	name: string | null;
	priority: 0 | 1;
	email: string;
	surveysNumber: number;
}, "id" | "priority" | "surveysNumber">;