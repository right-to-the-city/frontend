export type TypeOrNull<T> = T | null;

export type TypesOrNull<T, E extends keyof T = any> = Pick<T, E> & {
    [P in keyof Omit<T, E>]: TypeOrNull<T[P]>;
};