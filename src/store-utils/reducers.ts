import { loginReducer } from "@/pages/LoginPage/components/LoginForm/LoginForm.slice";
import { registrationReducer } from "@/pages/RegistrationPage/components/RegistrationForm/RegistrationForm.slice";
import { uploadStepsReducer } from "@/pages/UploadPage/components/Steps.slice";
import { mapReducer } from "@/pages/MapPage";
import { publicationsReducer } from "@/pages/PublicationsPage/components/PublicationList/PublicationList.slice";
import { publicationsReducer as myGradesReducer } from "@/pages/MyGradesPage/components/PublicationList/PublicationList.slice";
import { usersReducer } from "@/pages/AdminPanelPage/components/UsersTable.slice";
import { combineReducers } from "@reduxjs/toolkit";
import { userReducer } from "@/stores/user/user.slice";
import { myObjectsReducer } from "@/pages/MyObjectsPage";
import { analyticsRatingReducer } from "@/pages/ProfilePage/components/AnalyticsRating/AnalyticsRating.slice";
import { analyticsCategoryReducer } from "@/pages/ProfilePage/components/AnalyticsCategory/AnalyticsCategory.slice";
import { analyticsGeographyReducer } from "@/pages/ProfilePage/components/AnalyticsGeography/AnalyticsGeography.slice";

export const reducers = combineReducers({
	loginReducer,
	registrationReducer,
	uploadStepsReducer,
	mapReducer,
	publicationsReducer,
	usersReducer,
	userReducer,
	myGradesReducer,
	myObjectsReducer,
	analyticsRatingReducer,
	analyticsCategoryReducer,
	analyticsGeographyReducer
});
