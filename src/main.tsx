import ReactDOM from "react-dom/client";
import "leaflet/dist/leaflet.css";
import "./styles/common.scss";

import { App } from "./App";
import { store } from "./store-utils/store.ts";
import { Provider } from "react-redux";

ReactDOM.createRoot(document.getElementById("root")!).render(
	<Provider store={store}>
		<App />
	</Provider>,
);
