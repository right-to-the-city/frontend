import { appRouter } from "@/routes/routes.config";
import { useLayoutEffect } from "react";
import { RouterProvider } from "react-router-dom";

export const App = () => {
	useLayoutEffect(() => {
		window.history.replaceState({}, "");
	}, []);
	
	return (
		<div className="app">
			<RouterProvider router={appRouter} />
		</div>
	);
};
