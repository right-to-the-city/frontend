import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { User } from "@/types/user";
import { deleteFromLocalStorage, getLocalStorage, setLocalStorage } from "@/utils/localStorage";
import { TypeOrNull } from "@/types/utils";

type UserState = {
	user: TypeOrNull<User>;
}

export const USER_LOCAL_STORAGE_KEY = "user";

const initialState: UserState = { 
	user: getLocalStorage<User>(USER_LOCAL_STORAGE_KEY) || null
};

const userSlice = createSlice({
	name: "user-store",
	initialState,
	reducers: {
		setUser: (state, action: PayloadAction<TypeOrNull<User>>) => {
			state.user = action.payload;
			setLocalStorage(USER_LOCAL_STORAGE_KEY, action.payload);
		},
		removeUser: (state) => {
			state.user = null;
			deleteFromLocalStorage(USER_LOCAL_STORAGE_KEY);
		},
	},
});

export const { reducer: userReducer, actions: { setUser, removeUser } } = userSlice;
