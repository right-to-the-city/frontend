import { RootState } from "@/store-utils/store";
import { createSelector } from "@reduxjs/toolkit";

const selectState = ({ userReducer }: RootState) => userReducer;

export const selectUser = createSelector(selectState, ({ user }) => {
	return user; 
});
