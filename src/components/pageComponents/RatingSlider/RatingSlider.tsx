import { FC, memo } from "react";
import { FrownOutlined, SmileOutlined } from "@ant-design/icons";
import { Slider } from "antd";
import styles from "./RatingSlider.module.scss";
import clsx from "clsx";

const calculateOpacity = (value: number, min: number, max: number) => {
	const distanceToMin = Math.abs(value - min);
	const distanceToMax = Math.abs(value - max);
	const totalDistance = Math.abs(max - min);
	const opacityMin = 0.3;
	const opacityMax = 1;

	const opacityLeft = opacityMin + (opacityMax - opacityMin) * (distanceToMax / totalDistance);
	const opacityRight = opacityMin + (opacityMax - opacityMin) * (distanceToMin / totalDistance);

	return { opacityLeft, opacityRight };
};

type RatingSliderProps = {
  max: number;
  min: number;
  onChange: (value: number) => void;
  value: number;
	className?: string;
	disabled?: boolean;
}

export const RatingSlider: FC<RatingSliderProps> = memo(({ max, min, onChange, value, disabled = false, className }) => {
	const { opacityLeft, opacityRight } = calculateOpacity(value, min, max);

	return (
		<div className={clsx(styles.container, className)}>
			<FrownOutlined style={{ fontSize: "20px", opacity: opacityLeft }} />

			<Slider className={styles.slider} disabled={disabled} max={max} min={min} onChange={onChange} value={value} />
			
			<SmileOutlined style={{ fontSize: "20px", opacity: opacityRight }} />
		</div>
	);
});
