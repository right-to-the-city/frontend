import { LocationEvent } from "leaflet";
import { FC, memo, useEffect } from "react";
import { useMapEvents } from "react-leaflet";

type LocationMarkerProps = {
	setLocation?: (value: [lat: number, lng: number]) => void;
}

export const LocationMarker: FC<LocationMarkerProps> = memo(({ setLocation }) => {
	const map = useMapEvents({
		locationfound: (event: LocationEvent) => {      
			map.flyTo(event.latlng, map.getZoom());
			setLocation?.([event.latlng.lat, event.latlng.lng]);
		}
	});

	useEffect(() => {
		map.locate();
	}, []);

	return null;
});
