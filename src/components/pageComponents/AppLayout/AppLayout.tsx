
import { FC } from "react";
import { Outlet } from "react-router-dom";
import styles from "./AppLayout.module.scss";
import { Header } from "./Header/Header";

const AppLayout: FC = () => {
	return (
		<div className={styles.container}>
			<Header />

			<main className={styles.main}><Outlet /></main>
			
		</div>
	);
};

export default AppLayout;