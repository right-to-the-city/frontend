import { AppRoutes } from "@/routes/routes.constants";
import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";

const getActiveItemByPath = (pathname: string) => {
	return pathname.split("/").filter(Boolean).at(-1);
};

export const useActiveItem = () => {
	const { pathname } = useLocation();

	const [activeItem, setActiveItem] = useState(() => getActiveItemByPath(pathname));

	useEffect(() => {    
		const newActiveItem = getActiveItemByPath(pathname);

		if (newActiveItem === activeItem) {
			return;
		}

		setActiveItem(newActiveItem as AppRoutes);
    
	}, [pathname]);

	return { activeItem };
};