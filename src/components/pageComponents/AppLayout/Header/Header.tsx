import { FC } from "react";
import styles from "./Header.module.scss";
import { Button } from "@/components/shared/Button/Button";
import { routePath } from "@/routes/routes.constants";
import { useActiveItem } from "./hooks/useActiveItem";
import { SearchPlaceInput as MapSearchPlaceInput } from "@/pages/MapPage";
import { SearchPlaceInput as PublicationsSearchPlaceInput } from "@/pages/PublicationsPage/components/SearchPlaceInput/SearchPlaceInput";
import { SearchPlaceInput as MyGradesSearchPlaceInput } from "@/pages/MyGradesPage/components/SearchPlaceInput/SearchPlaceInput";
import { SearchPlaceInput as MyObjectsPagePlaceInput } from "@/pages/MyObjectsPage/components/SearchPlaceInput/SearchPlaceInput";
import { SelectCategoryFilter as MapSelectCategoryFilter } from "@/pages/MapPage";
import { SelectCategoryFilter as MyObjectsSelectCategoryFilter } from "@/pages/MyObjectsPage";
import Paper from "@/images/paper.svg";
import Map from "@/images/map.svg";
import Upload from "@/images/upload.svg";
import clsx from "clsx";
import { Dropdown, MenuProps } from "antd";
import { Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { selectUser } from "@/stores/user/user.selectors";
import { removeUser } from "@/stores/user/user.slice";

export const Header: FC = () => {
	const user = useAppSelector(selectUser);
	const dispatch = useAppDispatch();

	const { activeItem } = useActiveItem();
	
	const isMapPage = ["map", "heatMap"].includes(activeItem ?? "");

	const hasUser = !!user;
	const isAdmin = user?.priority === 1;

	const getItems = (isAdmin: boolean) => {
		return [
			{
				key: 1,
				label: <Link className={styles.dropdownLink} to={routePath.profile}>Личный кабинет</Link>,
			},
			isAdmin && {
				key: 2,
				label: <Link className={styles.dropdownLink} to={routePath.adminPanel}>Админ панель</Link>,
			},
			{
				key: 3,
				danger: true,
				label: <Link className={styles.dropdownLink} onClick={() => dispatch(removeUser())} to={routePath.login}>Выйти</Link>,
			},
		].filter(Boolean) as MenuProps["items"];
	};

	const dropDownItems = getItems(isAdmin);
	
	return (
		<header className={styles.container}>
			<div className={styles.content}>
				{isMapPage && <MapSearchPlaceInput />}
				{activeItem === "publications" && <PublicationsSearchPlaceInput />}
				{activeItem === "my-grades" && <MyGradesSearchPlaceInput />}
				{activeItem === "my-objects" && <MyObjectsPagePlaceInput />}
			
				<div className={styles.buttons}>
					{isMapPage && (
						<div className={styles.mapButtonContainer}>
							<Button className={clsx(styles.baseMapButton, styles.navbarItem)} theme={activeItem === "map" ? "blue" : "white"} linkState={{ notNeedFetch: true }} href={routePath.map}>базовая</Button>
							<Button className={clsx(styles.heatMapButton, styles.navbarItem)} theme={activeItem === "heatMap" ? "blue" : "white"} linkState={{ notNeedFetch: true }} href={routePath.heatMap}>Тепловая</Button>
						</div>
					)}
				
					{!isMapPage && <Button theme="white" href={routePath.map}>Карта <Map /></Button>}

					{activeItem !== "upload" && hasUser && <Button className={styles.navbarItem} theme="white" href={routePath.upload}>Загрузить место <Upload /></Button>}
					<Button theme={activeItem === "publications" ? "blue" : "white"} className={clsx(styles.publications, activeItem === "publications" && styles.isActiveItem, styles.navbarItem)} href={routePath.publications}>Лента <Paper /></Button>

					{hasUser && (
						<Dropdown trigger={["click"]} menu={{ items: dropDownItems }}>
							<Button onClick={(e) => e.preventDefault()} withTransition={false} title={user.name ?? undefined} theme="white" isSquare>{user.name?.[0].toUpperCase() ?? "A"}</Button>
						</Dropdown>
					)}

					{!hasUser && <Button href={routePath.login}>Войти</Button>}
				</div>

				{isMapPage && <MapSelectCategoryFilter className={styles.categoryFilter} />}
				{activeItem === "my-objects" && <MyObjectsSelectCategoryFilter className={styles.myObjectsCategoryFilter} />}
			</div>
		</header>
	);
};
