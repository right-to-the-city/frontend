import clsx from "clsx";
import styles from "./ObjectCard.module.scss";
import { MapObject } from "@/types/map";
import { formatDate } from "@/utils/date";
import { getSmileByObject } from "@/constants/rating";
import { useState } from "react";
import { ObjectModal } from "../ObjectModal/ObjectModal";

type ObjectCardProps = {
  className?: string;
  objectData: MapObject;
}

export const ObjectCard = ({ className, objectData }: ObjectCardProps) => {
	const [isOpen, setIsOpen] = useState(false);
	const firstUrl = objectData.media?.[0];
  
	const src = firstUrl ? `${import.meta.env.APP_BACKEND_URL}/api${firstUrl}` : "";

	const onOpen = () => {
		setIsOpen(true);
	};

	const onClose = () => {
		setIsOpen(false);
	};

	return (
		<>
			<div className={clsx(styles.container, className)} onClick={onOpen}>
				<div className={styles.imageContainer}>
					<img className={styles.img} src={src} alt="" />
					<div className={styles.smile}>{getSmileByObject(objectData)}</div>
				</div>
				<div className={styles.content}>
					<div className={styles.topContent}>
						<span className={styles.username}>{objectData.user.username}</span>
						<span className={styles.date}>{formatDate(objectData.date)}</span>
					</div>
					<div className={styles.comment}>{objectData.comment}</div>
					<div className={styles.categoryContainer}>Категория: <span className={styles.categoryName}>{objectData.category}</span></div>
					<div className={styles.tags}>{objectData.tags.map((tag) => (<>#{tag} </>))}</div>
				</div>
			</div>
			<ObjectModal isOpen={isOpen} objectData={objectData} onClose={onClose} /></>
	);
};
