import { FC } from "react";
import styles from "./ObjectModal.module.scss";
import { Carousel, Modal } from "antd";
import { MapObject } from "@/types/map";
import clsx from "clsx";
import { getSmileByObject } from "@/constants/rating";
import { formatDate } from "@/utils/date";

type ObjectModalProps = {
  objectData: MapObject;
  isOpen: boolean;
  onClose: () => void;
}

export const ObjectModal: FC<ObjectModalProps> = ({ objectData, isOpen, onClose }) => {
  
	return (
		<Modal width={1200} open={isOpen} onCancel={onClose} closable={false} footer={null}>
			<div className={clsx(styles.container)}>
				<div className={styles.imageContainer}>
					<Carousel draggable infinite={false} className={styles.carousel}>
						{objectData.media.map((url) => {
							const src = `${import.meta.env.APP_BACKEND_URL}/api${url}`;
							return <div className={styles.imageContainer} key={url}>
								<img className={styles.image} src={src} />
							</div>;
						})}
					</Carousel>
					<div className={styles.smile}>{getSmileByObject(objectData)}</div>
				</div>
				<div className={styles.content}>
					<div className={styles.topContent}>
						<span className={styles.username}>{objectData.user.username}</span>
						<span className={styles.date}>{formatDate(objectData.date)}</span>
					</div>
					<div className={styles.comment}>{objectData.comment}</div>
					<div className={styles.bottomContent}>
						<div className={styles.categoryContainer}>Категория: <span className={styles.categoryName}>{objectData.category}</span></div>
						<div className={styles.tags}>{objectData.tags.map((tag) => (<>#{tag} </>))}</div>
					</div>
				
				</div>
			</div>
		</Modal>
	);
};
