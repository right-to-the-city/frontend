import { InputHTMLAttributes, FC } from "react";
import styles from "./Input.module.scss";
import clsx from "clsx";

type InputProps = InputHTMLAttributes<HTMLInputElement>

export const Input: FC<InputProps> = ({ className, ...otherProps }) => {
	return (
		<input className={clsx(styles.input, className)} {...otherProps}/>
	);
};
