import { ButtonHTMLAttributes, FC, ReactNode } from "react";
import styles from "./Button.module.scss";
import { Link } from "react-router-dom";
import clsx from "clsx";

type ButtonProps = {
  children: ReactNode;
	href?: string;
	linkState?: any;
	theme?: "white" | "blue";
	isSquare?: boolean;
	withTransition?: boolean;
} & ButtonHTMLAttributes<HTMLButtonElement>

export const Button: FC<ButtonProps> = ({ children, href, withTransition = true, linkState, theme = "blue", className, isSquare = false, ...otherProps }) => {
	if (href) {
		return (
			<Link className={styles.link} to={href} state={linkState}>
				<button className={clsx(styles.button, styles[theme], isSquare && styles.isSquare, withTransition && styles.withTransition, className)} {...otherProps}>
					{children}
				</button>
			</Link>
		);
	}
	return (
		<button className={clsx(styles.button, styles[theme], isSquare && styles.isSquare, withTransition && styles.withTransition, className)} {...otherProps}>
			{children} 
		</button>
	);
};
