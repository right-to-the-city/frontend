import { Spin } from "antd";
import clsx from "clsx";
import { FC, memo } from "react";
import styles from "./LoaderWithFallback.module.scss";

type LoaderWithFallbackProps = {
  className?: string;
}

export const LoaderWithFallback: FC<LoaderWithFallbackProps> = memo(({ className }) => {
	return (
		<div className={clsx(styles.fallback, className)} >
			<Spin size="large" spinning />
		</div>
	);
});
