import { Button } from "@/components/shared/Button/Button";
import { FC, useState } from "react";
import ArrowsUp from "@/images/arrowsUp.svg";
import ArrowsDown from "@/images/arrowsDown.svg";

import styles from "./SelectCategoryFilter.module.scss";
import { ICON_COMPONENTS_BY_CATEGORIES } from "@/constants/categoryIcons";
import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { selectSelectedCategoryFilter } from "../../MapPage.selectors";
import clsx from "clsx";
import { setFilter } from "../../MapPage.slice";

type SelectCategoryFilterProps = {
  className?: string;
}
export const SelectCategoryFilter: FC<SelectCategoryFilterProps> = ({ className }) => {
	const [isExpanded, setIsExpanded] = useState(false);

	const dispatch = useAppDispatch();
	const selectedCategoryFilter = useAppSelector(selectSelectedCategoryFilter);
  
	const toggleSelectedCategory = (name: string) => {
		const isSelectedCategory = selectedCategoryFilter === name;

		dispatch(setFilter({ selectedRootCategory: isSelectedCategory ? null : name }));
	};

	const toggleExpanded = () => {
		setIsExpanded(prev => !prev);
	};

	return (
		<div className={className}>
			<div className={styles.list} style={{ height: isExpanded ? 608 : 0 }}>
				{
					Object.entries(ICON_COMPONENTS_BY_CATEGORIES).map(([categoryName, Icon]) => {
						const isSelectedCategory = selectedCategoryFilter === categoryName;
						const theme = isSelectedCategory ? "blue" : "white";

						return <Button onClick={() => toggleSelectedCategory(categoryName)} className={clsx(isSelectedCategory && styles.selectedCategory)} theme={theme} isSquare key={categoryName}>{Icon}</Button>;
					})
				}
			</div>
			<Button withTransition={false} onClick={toggleExpanded} theme="white" isSquare>{isExpanded ? <ArrowsUp /> : <ArrowsDown />}</Button>
		</div>
	);
};
