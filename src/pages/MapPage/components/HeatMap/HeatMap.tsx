import { useCallback, useEffect, useMemo, useState } from "react";
import { MapContainer, TileLayer } from "react-leaflet";
import { resizeXSScreenForMap } from "@/utils/map";
import { HeatmapLayerFactory } from "@vgrid/react-leaflet-heatmap-layer";
import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { objectsToShow, selectFoundLocation } from "../../MapPage.selectors";
import { LocationMarker } from "@/components/pageComponents/LocationMarkers";
import { setFoundLocation } from "../../MapPage.slice";
import { VOLGOGRAD_COORDS } from "@/constants/coords";
import { getRatingByObject } from "@/constants/rating";

const HeatmapLayer = HeatmapLayerFactory<[number, number, number]>();

const initKey = Math.random();

export const HeatMap = () => {
	const dispatch = useAppDispatch();
	
	const [key, setKey] = useState<number>(initKey);

	const objects = useAppSelector(objectsToShow);
	const foundLocation = useAppSelector(selectFoundLocation);

	const setLocation = useCallback((value: [lat: number, lng: number]) => {
		dispatch(setFoundLocation(value));
	}, []);

	const center = foundLocation ?? VOLGOGRAD_COORDS;

	const positions = useMemo<[number, number, number][]>(() => {
		return objects.map((object) => {
			return [object.lng, object.lat, getRatingByObject(object)];
		});
	}, [objects]);

	useEffect(() => {
		if (!positions.length) {
			return;
		}

		setKey(prev => prev + 1);
	}, [positions]);

	return (
		<MapContainer
			center={center}
			zoom={11}
			whenReady={resizeXSScreenForMap}
		>
			<HeatmapLayer
				key={key}
				points={positions}
				radius={10}
				
				longitudeExtractor={(m) => m[0]}
				latitudeExtractor={(m) => m[1]}
				intensityExtractor={() => 2}
			/>
			<TileLayer
				attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
				url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
			/>

			{!foundLocation && <LocationMarker setLocation={setLocation} />}		
		</MapContainer>
	);

};