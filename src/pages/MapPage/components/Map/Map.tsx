import { MapContainer, TileLayer } from "react-leaflet";
import { ObjectMarkers } from "./ObjectMarkers/ObjectMarkers";
import { LocationMarker } from "@/components/pageComponents/LocationMarkers";
import { resizeXSScreenForMap } from "@/utils/map";
import { useCallback } from "react";
import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { setFoundLocation } from "../../MapPage.slice";
import { selectFoundLocation } from "../../MapPage.selectors";
import { VOLGOGRAD_COORDS } from "@/constants/coords";

export const Map = () => {
	const dispatch = useAppDispatch();

	const foundLocation = useAppSelector(selectFoundLocation);

	const setLocation = useCallback((value: [lat: number, lng: number]) => {
		dispatch(setFoundLocation(value));
	}, []);

	const center = foundLocation ?? VOLGOGRAD_COORDS;

	return (
		<MapContainer
			center={center}
			zoom={13}
			whenReady={resizeXSScreenForMap}
		>
			<TileLayer
				attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
				url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
			/>
			{!foundLocation && <LocationMarker setLocation={setLocation} />}

			<ObjectMarkers />
		</MapContainer>
	);
};
