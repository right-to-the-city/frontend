import { Input } from "@/components/shared/Input/Input";
import RightToTheCityBlueIcon from "@/images/rightToTheCityBlue.svg";
import SearchIcon from "@/images/search.svg";
import styles from "./SearchPlaceInput.module.scss";
import { useAppDispatch } from "@/store-utils/hooks";
import { ChangeEvent } from "react";
import { setSearch } from "../../MapPage.slice";
import { debounce } from "@/utils/debounce";

export const SearchPlaceInput = () => {
	const dispatch = useAppDispatch();

	const onChange = debounce((event: ChangeEvent<HTMLInputElement>) => {
		dispatch(setSearch(event.target.value));
	});
	
	return (
		<div className={styles.container}>
			<div className={styles.mainIconContainer}>
				<RightToTheCityBlueIcon />
			</div>

			<Input onChange={onChange} className={styles.input} placeholder="Поиск мест"/>
      
			<div className={styles.searchIconContainer}>
				<SearchIcon />
			</div>
		</div>
	);
};
