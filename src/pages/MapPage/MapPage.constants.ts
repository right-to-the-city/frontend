export const baseThunkName = "map";

export const getPrefix = (name: string) => {
	return `${baseThunkName}/${name}`;
};
