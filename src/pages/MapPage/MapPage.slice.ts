import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { fetchObjectsOnMapAsync } from "./MapPage.thunks";
import { baseThunkName } from "./MapPage.constants";
import { MapObject } from "@/types/map";
import { TypeOrNull } from "@/types/utils";
import { LatLngExpression } from "leaflet";

type MapState = {
  objectsOnMap: MapObject[];
  selectedObjectOnMap: TypeOrNull<MapObject>;
  loading: boolean;
	hiddenLoading: boolean;
  foundLocation: LatLngExpression | null;
  error?: string;
	search: string;
	filter: {
		selectedRootCategory: string | null;
	};
}

const initialState: MapState = { 
	objectsOnMap: [],
	selectedObjectOnMap: null,
	loading: false,
	hiddenLoading: false,
	foundLocation: null,
	search: "",
	filter: {
		selectedRootCategory: null
	}
};

const mapSlice = createSlice({
	name: baseThunkName,
	initialState,
	reducers: {
		resetState: () => initialState,

		setSelectedObjectOnMap: (state, action: PayloadAction<TypeOrNull<MapObject>>) => {
			state.selectedObjectOnMap = action.payload;
		},
		setFoundLocation: (state, action: PayloadAction<LatLngExpression>) => {
			state.foundLocation = action.payload;
		},
		setSearch: (state, action: PayloadAction<string>) => {
			state.search = action.payload;
		},
		setFilter: (state, action: PayloadAction<Partial<MapState["filter"]>>) => {
			state.filter = { ...state.filter, ...action.payload } ;
		},
	},
	extraReducers: ({ addCase }) => {
		addCase(fetchObjectsOnMapAsync.pending, (state) => {
			if (!state.objectsOnMap?.length || state.loading) {
				state.loading = true;
				state.error = undefined;
				return;
			}

			state.hiddenLoading = true;
			state.error = undefined;
		});

		addCase(fetchObjectsOnMapAsync.fulfilled, (state, action) => {
			state.objectsOnMap = action.payload.data;
			state.error = undefined;
			state.loading = false;
			state.hiddenLoading = false;
		});

		addCase(fetchObjectsOnMapAsync.rejected, (state) => {
			state.error = "error";
			state.loading = false;
			state.hiddenLoading = false;
		});
	},
});

export const { 
	reducer: mapReducer, 
	actions: { 
		resetState,
		setSelectedObjectOnMap,
		setFoundLocation,
		setSearch,
		setFilter
	}
} = mapSlice;