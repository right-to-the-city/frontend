import { useCallback, useEffect } from "react";
import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { fetchObjectsOnMapAsync } from "./MapPage.thunks";
import { setSelectedObjectOnMap } from "./MapPage.slice";
import { selectIsHiddenLoading, selectIsLoading, selectSelectedObjectOnMap } from "./MapPage.selectors";
import { Map } from "./components/Map/Map";
import styles from "./MapPage.module.scss";
import { LoaderWithFallback } from "@/components/shared/LoaderWithFallback/LoaderWithFallback";
import { ObjectModal } from "@/components/shared/ObjectModal/ObjectModal";

const MapPage = () => {
	const dispatch = useAppDispatch();

	const selectedObjectOnMap = useAppSelector(selectSelectedObjectOnMap);
	const isLoading = useAppSelector(selectIsLoading);
	const isHiddenLoading = useAppSelector(selectIsHiddenLoading);

	useEffect(() => {
		if (isLoading || isHiddenLoading) {
			return;
		}
		
		dispatch(fetchObjectsOnMapAsync());
	}, []);

	const onClose = useCallback(() => {
		dispatch(setSelectedObjectOnMap(null));
	}, []);

	return (
		<div className={styles.container}>
			{isLoading && (
				<LoaderWithFallback />
			)}

			<Map />

			{selectedObjectOnMap && (
				<ObjectModal objectData={selectedObjectOnMap} isOpen={!!selectedObjectOnMap} onClose={onClose} />
			)}
		</div>
		
	);
};

export default MapPage;