import { Flex, Typography, FloatButton } from "antd";
import { RegistrationForm } from "./components/RegistrationForm/RegistrationForm";
import styles from "./RegistrationPage.module.scss";
import { Link } from "react-router-dom";
import { routePath } from "@/routes/routes.constants";
import { HeatMapOutlined } from "@ant-design/icons";

const RegistrationPage = () => {
	return (
		<Flex align="center" justify="center" vertical className={styles.container}>
			<Link to={routePath.map}><FloatButton tooltip={<div>Карта</div>} icon={<HeatMapOutlined />} /></Link>
			<Typography.Title className={styles.title} level={2}>Регистрация</Typography.Title>
			<RegistrationForm />
		</Flex>
	);
};

export default RegistrationPage;