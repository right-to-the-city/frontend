import { createSlice } from "@reduxjs/toolkit";
import { submitRegistrationFormAsync } from "./RegistrationForm.thunks";
import { baseThunkName } from "./RegistrationForm.constants";

type RegistrationState = {
  username: string;
  password: string;
  loading: boolean;
  error?: string;
}

const initialState: RegistrationState = { 
	username: "",
	password: "",
	loading: false 
};

const loginSlice = createSlice({
	name: baseThunkName,
	initialState,
	reducers: {},
	extraReducers: ({ addCase }) => {
		addCase(submitRegistrationFormAsync.pending, (state) => {
			state.loading = true;
			state.error = undefined;
		});

		addCase(submitRegistrationFormAsync.fulfilled, (state) => {
			state.error = undefined;
			state.loading = false;
		});

		addCase(submitRegistrationFormAsync.rejected, (state) => {
			state.error = "error";
		});
	},
});

export const { reducer: registrationReducer } = loginSlice;