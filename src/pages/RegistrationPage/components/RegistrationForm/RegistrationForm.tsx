import { memo } from "react";
import { Button, Form, Input, message } from "antd";
import styles from "./RegistrationForm.module.scss";
import { submitRegistrationFormAsync } from "./RegistrationForm.thunks";
import { useAppDispatch } from "@/store-utils/hooks";
import { Link, useNavigate } from "react-router-dom";
import { routePath } from "@/routes/routes.constants";
import { AxiosError } from "axios";
import { setUser } from "@/stores/user/user.slice";
import { User } from "@/types/user";

type onSubmitParams = {
	name: string;
	login: string;
	password: string;
	confirmPassword: string;
	email: string;
}

const requiredRule = { required: true, message: "Это поле обязательное!" };

export const RegistrationForm = memo(() => {
	const dispatch = useAppDispatch();

	const [messageApi, contextHolder] = message.useMessage();

	const navigate = useNavigate();

	const onSubmit = ({ confirmPassword, login, email, name }: onSubmitParams) => {
		dispatch(submitRegistrationFormAsync({ login, password: confirmPassword, email, name }))
			.then((response) => {
				if (response.meta.requestStatus === "fulfilled") {
					dispatch(setUser(response.payload as User));
					navigate(routePath.map);
				}
				if (response.meta.requestStatus === "rejected") {
					const content = (response.payload as AxiosError<{error: string}>)?.response?.data?.error ?? "Ошибка регистрации";
					messageApi.error({ content });
				}
				
			});
	};
  
	return (
		<Form
			name="basic"
			className={styles.container}
			layout="vertical"
			initialValues={{ remember: true }}
			onFinish={onSubmit}
			autoComplete="off"
		>
			{contextHolder}
			<Form.Item
				label="Имя"
				name="name"
				rules={[requiredRule]}
			>
				<Input />
			</Form.Item>

			<Form.Item
				label="Логин"
				name="login"
				rules={[requiredRule]}
			>
				<Input />
			</Form.Item>

			<Form.Item
				label="E-mail"
				name="email"
				rules={[{ ...requiredRule }, { type: "email", message: "Не валидный e-mail!" }]}
			>
				<Input type="email"/>
			</Form.Item>

			<Form.Item
				label="Пароль"
				name="password"
				rules={[requiredRule]}
				hasFeedback
			>
				<Input.Password />
			</Form.Item>

			<Form.Item
				label="Подтвердить пароль"
				name="confirmPassword"
				hasFeedback
				dependencies={["password"]}
				rules={[
					requiredRule,
					({ getFieldValue }) => ({
						validator(_, value) {
							if (!value || getFieldValue("password") === value) {
								return Promise.resolve();
							}

							return Promise.reject(new Error("Пароли не совпадают"));
						},
					}),
				]}
			>
				<Input.Password />
			</Form.Item>

			<Form.Item>
				<Button size="large" type="primary" htmlType="submit">
					Зарегистрироваться
				</Button>
				<div>Или <Link to={routePath.login}>Войти</Link></div>

			</Form.Item>
		</Form>
	);
});
