export const baseThunkName = "registrationForm";

export const getPrefix = (name: string) => {
	return `${baseThunkName}/${name}`;
};
