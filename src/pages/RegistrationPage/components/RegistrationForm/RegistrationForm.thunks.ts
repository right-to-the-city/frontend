import { apiPost } from "@/api/api";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { getPrefix } from "./RegistrationForm.constants";
import { User } from "@/types/user";

type SubmitRegistrationFormAsyncParams = {
	name: string;
  login: string;
  password: string;
  email: string;
}

export const submitRegistrationFormAsync = createAsyncThunk(
	getPrefix("registration"), 
	async ({ name, login, password, email }: SubmitRegistrationFormAsyncParams, { rejectWithValue }) => {
		try {
			const { data } = await apiPost<User>("/register", { login, password, name, email });
			return data; 
		} catch (error) {
			return rejectWithValue(error);
		}
	});