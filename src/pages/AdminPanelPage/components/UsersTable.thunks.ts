import { apiGet } from "@/api/api";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { getPrefix } from "./UsersTable.constants";
import { User } from "@/types/user";

export const fetchUsersAsync = createAsyncThunk(
	getPrefix("fetchUsers"), 
	async () => {
		const { data } = await apiGet<User[]>("/users");
		return data; 
	});
