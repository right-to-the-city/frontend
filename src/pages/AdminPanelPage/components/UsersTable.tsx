import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { ChangeEvent, memo, useEffect, useRef, useState } from "react";
import { selectIsLoading, selectUsers } from "./UsersTable.selectors";
import { fetchUsersAsync } from "./UsersTable.thunks";
import { resetState, setSearch } from "./UsersTable.slice";
import { Table, TableColumnsType } from "antd";
import styles from "./UsersTable.module.scss";
import { LoaderWithFallback } from "@/components/shared/LoaderWithFallback/LoaderWithFallback";
import { User } from "@/types/user";
import Search from "antd/es/input/Search";

const columns: TableColumnsType<User> = [
	{
		title: "Имя",
		dataIndex: "name",
		key: "name",
	},
	{
		title: "E-mail",
		dataIndex: "email",
		key: "email",
	},
	{
		title: "Username",
		dataIndex: "username",
		key: "username",
	},
	{
		title: "Количество публикаций",
		dataIndex: "surveysNumber",
		key: "surveysNumber",
		sorter: {
			compare: (a, b) => a.surveysNumber - b.surveysNumber,
		},
	},
];

export const UsersTable = memo(() => {
	const dispatch = useAppDispatch();
	const users = useAppSelector(selectUsers);
	const isLoading = useAppSelector(selectIsLoading);

	const [tableHeight, setTableHeight] = useState<number | undefined>(undefined);
	const tableRef = useRef<HTMLDivElement>(null);
  
	const showHeader = window.screen.width > 576;

	const onSearch = (value: string) => {
		dispatch(setSearch(value));
	};

	const onChange = (event: ChangeEvent<HTMLInputElement>) => {
		dispatch(setSearch(event.target.value));
	};

	useEffect(() => {
		dispatch(fetchUsersAsync());

		return () => {
			dispatch(resetState());
		};
	}, []);

	useEffect(() => {
		if (tableRef.current) {
			const tableHeight = tableRef.current.clientHeight - 115;
			setTableHeight(tableHeight);
		}
	}, [users]);

	return (
		<div className={styles.container}>
			<Search onChange={onChange} size="large" placeholder="Введите имя, e-mail или username" allowClear onSearch={onSearch} className={styles.search} />

			{isLoading && <LoaderWithFallback />}

			<div className={styles.tableContainer} ref={tableRef}>
				<Table locale={{ emptyText: "Пусто :(" }}
					showSorterTooltip={false}
					showHeader={showHeader}
					pagination={false}
					scroll={{ y: tableHeight, x: 100 }} 
					virtual 
					dataSource={users}
					columns={columns} />
			</div>
		</div>
	);
});
