import { RootState } from "@/store-utils/store";
import { createSelector } from "@reduxjs/toolkit";

const selectState = ({ usersReducer }: RootState) => usersReducer;

export const selectUsers = createSelector(selectState, ({ users, search }) => {
	if (!search) {
		return users;
	}

	const valueToSearch = search.trim().toLowerCase();

	return users.filter(user => {
		return user.name?.toLowerCase()?.includes(valueToSearch) 
		|| user.email?.toLowerCase()?.includes(valueToSearch) 
		|| user.username?.toLowerCase()?.includes(valueToSearch);
	});
});

export const selectIsLoading = createSelector(selectState, ({ loading }) => {
	return loading;
});
export const selectSearch = createSelector(selectState, ({ search }) => {
	return search;
});
