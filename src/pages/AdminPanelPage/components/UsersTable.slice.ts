import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { fetchUsersAsync } from "./UsersTable.thunks";
import { baseThunkName } from "./UsersTable.constants";
import { User } from "@/types/user";

type MapState = {
  users: User[];
	search: string;
  loading: boolean;
  error?: string;
}

const initialState: MapState = { 
	users: [],
	search: "",
	loading: false 
};

const usersSlice = createSlice({
	name: baseThunkName,
	initialState,
	reducers: {
		resetState: () => initialState,
		setSearch: (state, { payload }: PayloadAction<string>) => {
			state.search = payload;
		},
	},
	extraReducers: ({ addCase }) => {
		addCase(fetchUsersAsync.pending, (state) => {
			state.loading = true;
			state.error = undefined;
		});

		addCase(fetchUsersAsync.fulfilled, (state, action) => {
			state.users = action.payload;
			state.error = undefined;
			state.loading = false;
		});

		addCase(fetchUsersAsync.rejected, (state) => {
			state.error = "error";
		});
	},
});

export const { 
	reducer: usersReducer, 
	actions: { 
		resetState,
		setSearch
	}
} = usersSlice;