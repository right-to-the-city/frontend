export const baseThunkName = "users";

export const getPrefix = (name: string) => {
	return `${baseThunkName}/${name}`;
};
