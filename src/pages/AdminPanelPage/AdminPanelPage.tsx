import { UsersTable } from "./components/UsersTable";
import styles from "./AdminPanelPage.module.scss";

const AdminPanelPage = () => {
	return (
		<div className={styles.container}>
			<UsersTable />
		</div>
	);
};

export default AdminPanelPage;