export const baseThunkName = "publicationList";

export const getPrefix = (name: string) => {
	return `${baseThunkName}/${name}`;
};
