import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { FC, memo, useEffect } from "react";
import { selectIsLoading, selectObjects } from "./PublicationList.selectors";
import { fetchObjectsAsync } from "./PublicationList.thunks";

import styles from "./PublicationList.module.scss";
import { LoaderWithFallback } from "@/components/shared/LoaderWithFallback/LoaderWithFallback";
import AutoSizer from "react-virtualized-auto-sizer";
import { FixedSizeList } from "react-window";
import { ObjectCard } from "@/components/shared/ObjectCard/ObjectCard";

type RowProps = {
	index: number;
	style: any;
}

const ENDPOINTS = {
	2157: 4,
	1693: 3,
	1280: 2,
	854: 1
};

export const getCountCardInRow = () => {
	if (window.screen.width <= 2157) {
		return ENDPOINTS["2157"];
	}
	if (window.screen.width <= 1693) {
		return ENDPOINTS["1693"];
	}
	if (window.screen.width <= 1280) {
		return ENDPOINTS["1280"];
	}
	if (window.screen.width <= 854) {
		return ENDPOINTS["854"];
	}
	return 5;
};

const Row: FC<RowProps> = ({ index, style }) => {
	const objects = useAppSelector(selectObjects);

	if (!objects?.length) {
		return null;
	}

	const countInRow = getCountCardInRow();
	
	const startIdx = index * countInRow;
	const items = objects.slice(startIdx, startIdx + countInRow);

	if (!items.length) {
		return null;
	}

	return (
		<div className={styles.listContainer} style={style} key={index}>
			{items.map(item => (
				<ObjectCard objectData={item} key={item.id} />
			))}
		</div>
	);
};

export const PublicationList = memo(() => {
	const dispatch = useAppDispatch();

	const objects = useAppSelector(selectObjects);
	
	const isLoading = useAppSelector(selectIsLoading);
  
	useEffect(() => {
		const thunk = dispatch(fetchObjectsAsync());

		return () => {
			thunk.abort();
		};
	}, []);

	return (
		<div className={styles.container}>
			{isLoading && <LoaderWithFallback />}

			<AutoSizer>
				{({ width, height }) => {
					return (
						<FixedSizeList
							className={styles.list}
							height={height}
							itemCount={objects.length}
							itemSize={577}
							width={width}
						>
							{Row}
						</FixedSizeList>
					);
				} }
			</AutoSizer>
		</div>
	);
});
