import { apiGet } from "@/api/api";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { getPrefix } from "./PublicationList.constants";
import { MapObject } from "@/types/map";

export const fetchObjectsAsync = createAsyncThunk(
	getPrefix("fetchObjects"), 
	async () => {
		const { data } = await apiGet<{data: MapObject[]; total: number}>("/map");
		return data; 
	});
