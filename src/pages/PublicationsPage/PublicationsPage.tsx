import { memo } from "react";
import { PublicationList } from "./components/PublicationList/PublicationList";

const PublicationsPage = () => {
	return (
		<PublicationList />
	);
};

export default memo(PublicationsPage);