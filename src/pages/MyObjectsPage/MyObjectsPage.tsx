import { useCallback, useEffect } from "react";
import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { fetchObjectsOnMapAsync } from "./MyObjectsPage.thunks";
import { setSelectedObjectOnMap } from "./MyObjectsPage.slice";
import { selectIsLoading, selectSelectedObjectOnMap } from "./MyObjectsPage.selectors";
import { Map } from "./components/Map/Map";
import styles from "./MyObjectsPage.module.scss";
import { LoaderWithFallback } from "@/components/shared/LoaderWithFallback/LoaderWithFallback";
import { ObjectModal } from "@/components/shared/ObjectModal/ObjectModal";
import { selectUser } from "@/stores/user/user.selectors";

const MapPage = () => {
	const dispatch = useAppDispatch();

	const selectedObjectOnMap = useAppSelector(selectSelectedObjectOnMap);
	const isLoading = useAppSelector(selectIsLoading);
	const user = useAppSelector(selectUser);

	useEffect(() => {
		const thunks = dispatch(fetchObjectsOnMapAsync({ userId: user?.id }));

		return () => {
			thunks.abort();
		};
	}, [user?.id]);

	const onClose = useCallback(() => {
		dispatch(setSelectedObjectOnMap(null));
	}, []);

	return (
		<div className={styles.container}>
			{isLoading && (
				<LoaderWithFallback />
			)}
			<div className={styles.mapContainer}>
				<h1 className={styles.title}>Мои объекты</h1>
				<Map />

			</div>

			{selectedObjectOnMap && (
				<ObjectModal objectData={selectedObjectOnMap} isOpen={!!selectedObjectOnMap} onClose={onClose} />
			)}
		</div>
		
	);
};

export default MapPage;