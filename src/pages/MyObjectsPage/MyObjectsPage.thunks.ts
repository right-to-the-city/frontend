import { apiGet, apiPost } from "@/api/api";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { getPrefix } from "./MyObjectsPage.constants";
import { MapObject } from "@/types/map";
import { RootState } from "@/store-utils/store";
import { selectUser } from "@/stores/user/user.selectors";

type FetchObjectsOnMapAsyncParams = {
	userId?: number;
}

export const fetchObjectsOnMapAsync = createAsyncThunk(
	getPrefix("map"), 
	async ({ userId }: FetchObjectsOnMapAsyncParams) => {
		const { data } = await apiGet<{ data: MapObject[]; total: number }>("/map", { params: { userId } });
		return data; 
	});

type AddCommentToObjectAsyncResponse = {
	comment_id: number;
	comment_text: string;
	user_id: number;
	survey_id: number;
	date: string;
}

type AddCommentToObjectAsyncParams = {
	comment: string;
	surveyId: number;
}

export const addCommentToObjectAsync = createAsyncThunk(
	getPrefix("addCommentToObject"), 
	async ({ comment, surveyId, }: AddCommentToObjectAsyncParams, { getState }) => {
		const state = getState() as RootState;
		const user = selectUser(state);

		if (!user?.id) {
			return;
		}

		const { data } = await apiPost<AddCommentToObjectAsyncResponse>("/add_comment", { comment, surveyId, userId: user.id });
		return data; 
	});