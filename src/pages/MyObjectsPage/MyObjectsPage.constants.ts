export const baseThunkName = "myObjects";

export const getPrefix = (name: string) => {
	return `${baseThunkName}/${name}`;
};
