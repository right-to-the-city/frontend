import { ROOT_CATEGORY_BY_CHILDREN } from "@/constants/categories";
import { RootState } from "@/store-utils/store";
import { createSelector } from "@reduxjs/toolkit";

const selectState = ({ myObjectsReducer }: RootState) => myObjectsReducer;

export const selectObjectsOnMap = createSelector(selectState, ({ objectsOnMap }) => {
	return objectsOnMap;
});
export const selectSelectedObjectOnMap = createSelector(selectState, ({ selectedObjectOnMap }) => {
	return selectedObjectOnMap;
});
export const selectIsLoading = createSelector(selectState, ({ loading }) => {
	return loading;
});
export const selectIsHiddenLoading = createSelector(selectState, ({ hiddenLoading }) => {
	return hiddenLoading;
});
export const selectLatLngPoints = createSelector(selectState, ({ loading }) => {
	return loading;
});
export const selectFoundLocation = createSelector(selectState, ({ foundLocation }) => {
	return foundLocation;
});
export const selectSelectedCategoryFilter = createSelector(selectState, ({ filter }) => {
	return filter.selectedRootCategory;
});
export const objectsToShow = createSelector(selectState, ({ search, objectsOnMap, filter }) => {
	if (!search && !filter.selectedRootCategory) {
		return objectsOnMap; 
	}

	return objectsOnMap.filter((object) => {
		const validValuesToSearch = Object.values(object).filter(Boolean);
		const isSearchedBySearch = !!search && JSON.stringify(validValuesToSearch).includes(search);
		const isSearchedByFilter = ROOT_CATEGORY_BY_CHILDREN?.[object.category as keyof typeof ROOT_CATEGORY_BY_CHILDREN] === filter.selectedRootCategory;
		
		return isSearchedByFilter || isSearchedBySearch;
	});
});