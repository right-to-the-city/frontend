import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { objectsToShow } from "../../../MyObjectsPage.selectors";
import { memo, useMemo } from "react";
import { DivIcon, LatLng } from "leaflet";
import { Marker } from "react-leaflet";
import MarkerClusterGroup from "react-leaflet-cluster";
import { setSelectedObjectOnMap } from "../../../MyObjectsPage.slice";
import defaultMarkerSrc from "@/images/markers/default.png";
import { ICON_BY_CATEGORIES } from "@/constants/categoryIcons";
import { ROOT_CATEGORY_BY_CHILDREN } from "@/constants/categories";

export const ObjectMarkers = memo(() => {
	const dispatch = useAppDispatch();
	const objects = useAppSelector(objectsToShow);
  
	const positions = useMemo<LatLng[]>(() => {
		return objects.map((object) => {
			return new LatLng(object.lat, object.lng);
		});
	}, [objects]);

	const markers = useMemo(() => (positions.map((position, index) => {
		const object = objects[index];
		const onClickMarket = () => {
			dispatch(setSelectedObjectOnMap(object));
		};
		const rootCategory = ROOT_CATEGORY_BY_CHILDREN?.[object.category as keyof typeof ROOT_CATEGORY_BY_CHILDREN] as keyof typeof ICON_BY_CATEGORIES;
		const iconSrc = ICON_BY_CATEGORIES?.[rootCategory] ?? defaultMarkerSrc; 

		const icon = new DivIcon({
			html: `<img loading="lazy" src="${iconSrc}"/>`,
			className: "png-custom-marker",
			iconSize: [14, 14],
		});
		
		return <Marker icon={icon} eventHandlers={{ click: onClickMarket }} key={object.id} position={position} />;
	})), [positions]);
 
	return (
		<MarkerClusterGroup disableClusteringAtZoom={18} removeOutsideVisibleBounds chunkedLoading>
			{markers}
		</MarkerClusterGroup>
	);
});
