import { Steps } from "./components/Steps";
import styles from "./UploadPage.module.scss";

const UploadPage = () => {
	return (
		<div className={styles.container}>
			<Steps />
		</div>
	);
};

export default UploadPage;