import Dragger from "antd/es/upload/Dragger";
import { InboxOutlined } from "@ant-design/icons";
import { memo, useMemo } from "react";
import { UploadFile, UploadProps } from "antd";
import styles from "./UploadFilesStep.module.scss";
import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { setFiles } from "../Steps.slice";
import { selectFiles } from "../Steps.selectors";
import { RcFile } from "antd/es/upload";

export const UploadFilesStep = memo(() => {
	const dispatch = useAppDispatch();

	const files = useAppSelector(selectFiles);

	const defaultFileList = useMemo<UploadFile[]>(() => {
		return files.map((file) => {
			return { 
				name: file.name,
				fileName: file.name,
				uid: file.uid,
				originFileObj: file
			};
		});
	}, [files]);

	//@ts-ignore
	const props = useMemo<UploadProps>(() => {
		return {
			name: "file",
			multiple: true,
			maxCount: 10,
			accept: "image/*",
			defaultFileList,
			className: styles.dragger,
			customRequest: (options) => {
				options.onSuccess?.({});
			},
			onChange: (info) => {
				const files = info.fileList
					.filter(({ originFileObj }) => Boolean(originFileObj))
					.map(({ originFileObj }) => originFileObj);
				
				dispatch(setFiles(files as RcFile[]));
			}
		};
	}, [defaultFileList]);

	return (
		<div className={styles.container}>
			<Dragger {...props}>
				<p className="ant-upload-drag-icon">
					<InboxOutlined />
				</p>

				<p className="ant-upload-text">Загрузите фотографии (от 1 до 10 штук)</p>
			</Dragger>
		</div>
	);
});
