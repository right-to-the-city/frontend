import React, { useEffect, useRef, useState, memo, FC } from "react";
import type { InputRef } from "antd";
import { Input, Tag, theme } from "antd";
import { TweenOneGroup } from "rc-tween-one";
import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { selectTags } from "../Steps.selectors";
import { addTags, removeTags } from "../Steps.slice";

//TODO: копипаст - исправить

type TagInputFieldProps = {
	readOnly: boolean;
}

export const TagInputField: FC<TagInputFieldProps> = memo(({ readOnly = false }) => {
	const { token } = theme.useToken();

	const dispatch = useAppDispatch();
	const tags = useAppSelector(selectTags);
	
	const [inputVisible, setInputVisible] = useState(false);
	const [inputValue, setInputValue] = useState("");

	const inputRef = useRef<InputRef>(null);

	const onClose = (removedTag: string) => {
		if (readOnly) {
			return;
		}

		dispatch(removeTags(removedTag));
	};

	const showInput = () => {
		if (readOnly) {
			return;
		}
		setInputVisible(true);
	};

	const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		setInputValue(e.target.value);
	};

	const onInputConfirm = () => {
		if (inputValue && tags.indexOf(inputValue) === -1) {
			dispatch(addTags(inputValue));
		}
		setInputVisible(false);
		setInputValue("");
	};

	useEffect(() => {
		if (inputVisible) {
			inputRef.current?.focus();
		}
	}, [inputVisible]);

	const forMap = (tag: string) => (
		<span key={tag} style={{ display: "inline-block", height: "77px", }}>
			<Tag
				style={{ height: "77px", display: "flex", alignItems: "center", padding: "0px 20px" }}
				closable
				onClose={(e) => {
					e.preventDefault();
					onClose(tag);
				}}
			>
				{tag}
			</Tag>
		</span>
	);

	const tagChild = tags.map(forMap);

	const tagPlusStyle: React.CSSProperties = {
		background: token.colorBgContainer,
		borderStyle: "dashed",
		width: "223px",
		height: "77px",
		color: "#A0A0A0",
		display: "flex",
		alignItems: "center",
		justifyContent: "center"
	};

	return (
		<>
			<div style={{ marginBottom: 16 }}>
				<TweenOneGroup
					appear={false}
					enter={{ scale: 0.8, opacity: 0, type: "from", duration: 100 }}
					leave={{ opacity: 0, width: 0, scale: 0, duration: 200 }}
					onEnd={(e) => {
						if (e.type === "appear" || e.type === "enter") {
							(e.target as any).style = "display: inline-block";
						}
					}}
				>
					{tagChild}
				</TweenOneGroup>
			</div>
			{inputVisible ? (
				<Input
					ref={inputRef}
					type="text"
					size="small"
					style={{ width: 223, height: 77 }}
					value={inputValue}
					onChange={onInputChange}
					onBlur={onInputConfirm}
					onPressEnter={onInputConfirm}
				/>
			) : (
				<Tag onClick={showInput} style={tagPlusStyle}>
					Добавить тег +
				</Tag>
			)}
		</>
	);
});
