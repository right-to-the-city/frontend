import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { Form } from "antd";
import TextArea from "antd/es/input/TextArea";
import { ChangeEvent, memo, useCallback } from "react";
import { selectComment, selectIsLoading } from "../Steps.selectors";
import { setComment } from "../Steps.slice";
import { TagInputField } from "./TagInputField";

export const AddCommentStep = memo(() => {
	const comment = useAppSelector(selectComment);
	const isLoading = useAppSelector(selectIsLoading);

	const dispatch = useAppDispatch();

	const onChangeComment = useCallback((event: ChangeEvent<HTMLTextAreaElement>) => {
		dispatch(setComment(event.target.value));
	}, []);

	return (
		<div>
			<Form.Item>
				<TagInputField readOnly={isLoading} />
			</Form.Item>

			<Form.Item>
				<TextArea readOnly={isLoading} style={{ resize: "none" }} placeholder="Комментарий" rows={8} onChange={onChangeComment} value={comment ?? ""} />
			</Form.Item>
		</div>
	);
});
