export const baseThunkName = "uploadSteps";

export const getPrefix = (name: string) => {
	return `${baseThunkName}/${name}`;
};
