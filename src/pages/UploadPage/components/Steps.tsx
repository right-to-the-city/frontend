import { useCallback, useEffect, useMemo, useState } from "react";
import { Steps as AtndSteps, Flex, message } from "antd";
import { SelectLocationStep } from "./SelectLocationStep/SelectLocationStep";
import { SelectCategoriesStep } from "./SelectCategoriesStep/SelectCategoriesStep";
import { UploadFilesStep } from "./UploadFilesStep/UploadFilesStep";
import { AddCommentStep } from "./AddCommentStep/AddCommentStep";
import styles from "./Steps.module.scss";
import { publishObjectAsync } from "./Steps.thunks";
import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { resetState } from "./Steps.slice";
import { selectIsDisabledToNext, selectIsLoading } from "./Steps.selectors";
import { RatingStep } from "./RatingStep/RatingStep";
import { Button } from "@/components/shared/Button/Button";

const ITEMS = [
	{
		title: "Локация",
		content: <SelectLocationStep />
	},
	{
		title: "Категория",
		content: <SelectCategoriesStep />,
	},
	{
		title: "Фото",
		content: <UploadFilesStep />,
	},
	{
		title: "Оценка",
		content: <RatingStep />,
	},
	{
		title: "Комментарий",
		content: <AddCommentStep />,
	},
];

const messageKey = "publish";

export const Steps = () => {
	const dispatch = useAppDispatch();

	const [current, setCurrent] = useState<number>(0);

	const [messageApi, contextHolder] = message.useMessage();

	const isLoading = useAppSelector(selectIsLoading);
	const isDisabledToNext = useAppSelector(selectIsDisabledToNext(current));

	const goToPrev = useCallback(() => {
		setCurrent((prev) => prev - 1);
	}, []);

	const goToNext = useCallback(() => {
		setCurrent((prev) => prev + 1);
	}, []);

	const goToPublish = useCallback(() => {
		messageApi.open({
			key: messageKey,
			type: "loading",
			content: "Публикуется...",
		});
		
		dispatch(publishObjectAsync()).then((response) => {
			if (response.meta.requestStatus === "fulfilled") {
				messageApi.open({
					key: messageKey,
					type: "success",
					content: "Объект опубликован!",
					duration: 3,
				});
			}

			if (response.meta.requestStatus === "rejected") {
				messageApi.open({
					key: messageKey,
					type: "error",
					content: "Произошла ошибка при публикации объекта!",
					duration: 3,
				});
			}
			
			setCurrent(0);
			
			dispatch(resetState());
		});
		
	}, []);

	const stepItems = useMemo(() => {
		return ITEMS.map((item) => ({ key: item.title, title: item.title }));
	}, []);

	useEffect(() => {
		return () => {
			dispatch(resetState());
		};
	}, []);

	return (
		<>
			{contextHolder}

			<Flex className={styles.container} vertical>
				<AtndSteps className={styles.steps} current={current} items={stepItems}/>

				<div className={styles.content}>
					{ITEMS[current].content}
				</div>

				<Flex gap={10} className={styles.bottomButtons}>
					{current > 0 && (
						<Button withTransition={false} theme="white" disabled={isLoading} className={styles.bottomButton} onClick={goToPrev}>
							Назад
						</Button>
					)}

					{current < ITEMS.length - 1 && (
						<Button withTransition={false} theme="blue" disabled={isDisabledToNext} className={styles.bottomButton} onClick={goToNext}>
							Продолжить  
						</Button>
					)}

					{current === ITEMS.length - 1 && (
						<Button withTransition={false} disabled={isDisabledToNext} className={styles.bottomButton} onClick={goToPublish}>
							Опубликовать
						</Button>
					)}
				</Flex>
			
			</Flex>
		</>
		
	);
};
