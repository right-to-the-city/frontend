import { RootState } from "@/store-utils/store";
import { createSelector } from "@reduxjs/toolkit";

const selectState = ({ uploadStepsReducer }: RootState) => uploadStepsReducer;

export const selectCategoryName = createSelector(selectState, ({ category }) => {
	return category.name; 
});
export const selectCategoryBreadcrumbs = createSelector(selectState, ({ category }) => {
	return category.breadcrumbs; 
});
export const selectSingleLevelCategories = createSelector(selectState, ({ category }) => {
	return category.singleLevelCategories; 
});
export const selectCoordinates = createSelector(selectState, ({ coordinates }) => {
	return coordinates;
});
export const selectFiles = createSelector(selectState, ({ files }) => {
	return files;
});
export const selectComment = createSelector(selectState, ({ comment }) => {
	return comment;
});
export const selectTags = createSelector(selectState, ({ tags }) => {
	return tags;
});
export const selectRating = createSelector(selectState, ({ rating }) => {
	return rating;
});
export const selectIsLoading = createSelector(selectState, ({ loading }) => {
	return loading;
});
export const selectIsDisabledToNext = (page: number) => createSelector(selectState, ({ coordinates, files, category, rating, loading, comment }) => {	
	switch (page) {
	case 0:
		return !coordinates.lat && !coordinates.lng;
	case 1:
		return !category.name;
	case 2:
		return !files.length;		
	case 3:
		return rating === 0;		
	case 4:
		return loading || !comment?.length;
	default:
		return false;
	}
});