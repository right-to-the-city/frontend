import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { publishObjectAsync } from "./Steps.thunks";
import { baseThunkName } from "./Steps.constants";
import { TypeOrNull } from "@/types/utils";
import { RcFile } from "antd/es/upload";
import { CATEGORIES, Category } from "@/constants/categories";
import { CategoryBreadcrumb } from "./Steps.types";

type UploadStepsState = {
  coordinates: {
    lat: TypeOrNull<number>;
    lng: TypeOrNull<number>;
  };
  category: {
		name: TypeOrNull<string>;
		breadcrumbs: CategoryBreadcrumb[]; 
		singleLevelCategories: Category[];
	};
  comment: TypeOrNull<string>;
  rating: number;
  tags: string[];
	files: RcFile[];
  loading: boolean;
  error?: string;
}

export const initialCategoryBreadcrumbs = [{ title: "Все категории", id: "0" }];

const initialState: UploadStepsState = { 
	category: {
		name: null,
		breadcrumbs: initialCategoryBreadcrumbs,
		singleLevelCategories: CATEGORIES
	},
	comment: null,
	coordinates: {
		lat: null,
		lng: null
	},
	tags: [],
	files: [],
	rating: 0,
	loading: false 
};

const uploadStepsSlice = createSlice({
	name: baseThunkName,
	initialState,
	reducers: {
		resetState: () => initialState,

		setCategoryName: (state, action: PayloadAction<string>) => {
			state.category.name = action.payload;
		},
		setCategoryBreadcrumbs: (state, action: PayloadAction<CategoryBreadcrumb[]>) => {
			state.category.breadcrumbs = action.payload;
		},
		addCategoryBreadcrumbs: (state, action: PayloadAction<CategoryBreadcrumb>) => {
			state.category.breadcrumbs.push(action.payload);
		},
		setSingleLevelCategories: (state, action: PayloadAction<Category[]>) => {
			state.category.singleLevelCategories = action.payload;
		},
		
		setCoordinates: (state, action: PayloadAction<{lat: number; lng: number}>) => {
			state.coordinates = action.payload;
		},

		setFiles: (state, action: PayloadAction<RcFile[]>) => {
			state.files = action.payload;
		},

		setComment: (state, action: PayloadAction<string>) => {
			state.comment = action.payload;
		},

		addTags: (state, action: PayloadAction<string>) => {
			state.tags.push(action.payload);
		},

		removeTags: (state, action: PayloadAction<string>) => {
			state.tags = state.tags.filter((tag) => tag !== action.payload);
		},

		setRating: (state, action: PayloadAction<number>) => {
			state.rating = action.payload;
		},
	},
	extraReducers: ({ addCase }) => {
		addCase(publishObjectAsync.pending, (state) => {
			state.loading = true;
			state.error = undefined;
		});

		addCase(publishObjectAsync.fulfilled, (state) => {
			state.error = undefined;
			state.loading = false;
		});

		addCase(publishObjectAsync.rejected, (state) => {
			state.error = "error";
		});
	},
});

export const { 
	reducer: uploadStepsReducer, 
	actions: { 
		setCategoryName,
		setCoordinates,
		resetState,
		setFiles,
		setComment,
		addTags,
		removeTags,
		setRating,
		setSingleLevelCategories,
		setCategoryBreadcrumbs,
		addCategoryBreadcrumbs
	}
} = uploadStepsSlice;