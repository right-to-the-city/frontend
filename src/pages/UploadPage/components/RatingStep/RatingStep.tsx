import { RatingSlider } from "@/components/pageComponents/RatingSlider/RatingSlider";
import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { memo, useCallback } from "react";
import { selectRating } from "../Steps.selectors";
import { setRating } from "../Steps.slice";
import styles from "./RatingStep.module.scss";
import { Typography } from "antd";
import { SMILE_BY_RATING } from "@/constants/rating";

export const RatingStep = memo(() => {
	const dispatch = useAppDispatch();

	const rating = useAppSelector(selectRating);

	const onChange = useCallback((value: number) => {
		dispatch(setRating(value));
	}, []);

	return (
		<div>
			<Typography.Title className={styles.title} level={3}>Выставите оценку от -5 до 5</Typography.Title>
			<div className={styles.smile}>{SMILE_BY_RATING[rating]}</div>
			<RatingSlider className={styles.slider} max={5} min={-5} onChange={onChange} value={rating} />
		</div>
	);
});
