import { Card as AntdCard } from "antd";
import clsx from "clsx";
import { FC, memo, useCallback } from "react";
import styles from "./Card.module.scss";

type CardProps = {
  onClick: (value: string) => void;
  isSelected: boolean;
  name: string;
}

export const Card: FC<CardProps> = memo(({ onClick, isSelected, name }) => {
	const onClickHandler = useCallback(() => {		
		onClick(name);
	}, [name]);

	return (
		<AntdCard
			onClick={onClickHandler}
			className={clsx(styles.card, isSelected && styles.isSelected)}>
			{name}
		</AntdCard> 
	);
});
