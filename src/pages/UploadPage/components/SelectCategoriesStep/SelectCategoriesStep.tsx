import { memo } from "react";
import styles from "./SelectCategoriesStep.module.scss";
import { addCategoryBreadcrumbs, initialCategoryBreadcrumbs, setCategoryBreadcrumbs, setCategoryName, setSingleLevelCategories } from "../Steps.slice";
import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { selectCategoryBreadcrumbs, selectCategoryName, selectSingleLevelCategories } from "../Steps.selectors";
import { CATEGORIES, Category } from "@/constants/categories";
import { Breadcrumb } from "antd";
import { Card } from "./Card/Card";
import { findCategoryById } from "./utils";
import { CategoryBreadcrumb } from "../Steps.types";

export const SelectCategoriesStep = memo(() => {
	const dispatch = useAppDispatch();

	const categoryName = useAppSelector(selectCategoryName);
	const breadcrumbs = useAppSelector(selectCategoryBreadcrumbs);
	const singleLevelCategories = useAppSelector(selectSingleLevelCategories);

	const onClickCard = (category: Category) => {
		if (!category.children) {
			dispatch(setCategoryName(category.name));
		} else {
			dispatch(addCategoryBreadcrumbs({ id: category.id, title: category.name }));
			dispatch(setSingleLevelCategories(category.children));
		}
	};

	const onClickBreadcrumb = ({ breadcrumb, index }: {breadcrumb: CategoryBreadcrumb; index: number }) => {
		if (breadcrumb.id === "0") {
			dispatch(setCategoryBreadcrumbs(initialCategoryBreadcrumbs));
			dispatch(setSingleLevelCategories(CATEGORIES));
			return;
		}

		const foundCategory = findCategoryById(breadcrumb.id);
	
		if (!foundCategory || !foundCategory.children) {
			return;
		}

		dispatch(setCategoryBreadcrumbs(breadcrumbs.slice(0, index + 1)));
		dispatch(setSingleLevelCategories(foundCategory.children));
	};

	return (
		<div className={styles.content} >
			<Breadcrumb className={styles.breadcrumbs}>
				{breadcrumbs.map((item, index) => {
					return <Breadcrumb.Item className={styles.breadcrumbItem} key={item.id} onClick={() => onClickBreadcrumb({ breadcrumb: item, index })}>{item.title}</Breadcrumb.Item>;
				})}
			</Breadcrumb>

			<div className={styles.categories} >
				{singleLevelCategories.map((category) => {
					return (
						<Card key={category.name} isSelected={categoryName === category.name} name={category.name} onClick={() => onClickCard(category)} />
					); 
				})}
			</div>
		</div>

	);
});
