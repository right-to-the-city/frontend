import { CATEGORIES, Category } from "@/constants/categories";

export const findCategoryById = (categoryId: string) => {
	const findCategoryRecursive = (categories: Category[], id: string): Category | undefined => {
		for (const category of categories) {
			if (category.id === id) {
				return category;
			}
			if (category.children) {
				const found = findCategoryRecursive(category.children, id);
				if (found) {
					return found;
				}
			}
		}
		return undefined;
	};

	return findCategoryRecursive(CATEGORIES, categoryId);
};
