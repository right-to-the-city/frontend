import { Flex, Typography } from "antd";
import { FC, memo } from "react";
import { Card } from "../Card/Card";
import { Category } from "@/constants/categories";
import styles from "./Row.module.scss";
import { TypeOrNull } from "@/types/utils";

type RowProps = {
  category: Category;
  selectedCategoryName: TypeOrNull<string>;
  onSelect: (value: string) => void;
	depth: number;
	rowClassName?: string;
}

export const Row: FC<RowProps> = memo(({ category, selectedCategoryName, onSelect, rowClassName, depth }) => {  
	if (!category.children) {
		return (
			<Card
				onClick={onSelect}
				name={category.name}
				isSelected={selectedCategoryName === category.name}
			/>
		);
	}

	const titleLevel = depth > 3 ? 5 : depth + 3 as 1 | 2 | 3 | 4 | 5 | undefined;

	const startGap = window.screen.width <= 576 ? 16 : 24;
	const minGap = window.screen.width <= 576 ? 4 : 16;

	const gap = depth > 3 ? minGap : startGap - (depth * 8);

	const newDepth = depth + 1;
  
	return (
		<div className={rowClassName}>
			<Typography.Title className={styles.title} level={titleLevel}>{category.name}</Typography.Title>
										
			<Flex wrap="wrap" className={styles.row} gap={gap} >
				{category.children.map((nestedCategory) => {
					return (
						<Row
							key={nestedCategory.name}
							category={nestedCategory}
							onSelect={onSelect}
							selectedCategoryName={selectedCategoryName}
							rowClassName={rowClassName}
							depth={newDepth}
						/>
					);
				})}
			</Flex>
		</div>
	);
});
