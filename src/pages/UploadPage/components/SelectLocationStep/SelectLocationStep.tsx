import { MapContainer, TileLayer } from "react-leaflet";
import { LocationMarker } from "./LocationMarkers";
import { resizeXSScreenForMap } from "@/utils/map";

export const SelectLocationStep = () => {	
	return (
		<MapContainer
			center={[48.7194, 44.5018]}
			zoom={13}
			whenReady={resizeXSScreenForMap}
		>
			<TileLayer
				attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
				url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
			/>

			<LocationMarker />
		</MapContainer>
	);
};
