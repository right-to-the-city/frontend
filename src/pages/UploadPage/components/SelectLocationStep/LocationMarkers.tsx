import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { DivIcon, LatLng, LeafletMouseEvent, LocationEvent } from "leaflet";
import { memo, useEffect } from "react";
import { Marker, useMapEvents } from "react-leaflet";
import { selectCoordinates } from "../Steps.selectors";
import { setCoordinates } from "../Steps.slice";
import defaultMarkerSrc from "@/images/markers/default.png";

export const LocationMarker = memo(() => {

	const coordinates = useAppSelector(selectCoordinates);

	const dispatch = useAppDispatch();

	const map = useMapEvents({
		click: (event: LeafletMouseEvent) => {

			const { lat, lng } = event.latlng;

			dispatch(setCoordinates({ lat, lng }));
		},
		locationfound: (event: LocationEvent) => {      
			map.flyTo(event.latlng, map.getZoom());
		}
	});

	useEffect(() => {
		map.locate();
	}, []);

	if (!coordinates.lat || !coordinates.lng) {
		return null;
	}
	const position = new LatLng(coordinates.lat, coordinates.lng);
	
	const icon = new DivIcon({
		html: `<img src="${defaultMarkerSrc}"/>`,
		className: "png-custom-marker",
		iconSize: [14, 14],
	});
  
	return <Marker icon={icon} position={position} />;
});
