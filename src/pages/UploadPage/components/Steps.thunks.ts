import { apiPost } from "@/api/api";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { getPrefix } from "./Steps.constants";
import { RootState } from "@/store-utils/store";
import { selectCategoryName, selectComment, selectCoordinates, selectFiles, selectRating, selectTags } from "./Steps.selectors";
import { selectUser } from "@/stores/user/user.selectors";

export const publishObjectAsync = createAsyncThunk(
	getPrefix("publishObject"), 
	async (_, { getState, rejectWithValue }) => {
		const state = getState() as RootState;

		const user = selectUser(state);
		const categoryName = selectCategoryName(state);
		const coordinates = selectCoordinates(state);
		const files = selectFiles(state);
		const comment = selectComment(state);
		const tags = selectTags(state);
		const rating = selectRating(state);

		if (!categoryName || !coordinates.lat || !coordinates.lng || !comment || !user) {
			return rejectWithValue("Не все поля заполнены");
		}

		const formData = new FormData();
		formData.append("categoryName", categoryName);
		formData.append("lat", coordinates.lat.toString());
		formData.append("lon", coordinates.lng.toString());

		files.forEach((file, index) => {
			formData.append(`files[${index}]`, file);
		});

		formData.append("tags", tags.join(","));

		formData.append("userId", user.id.toString());

		formData.append("rating", rating.toString());

		formData.append("comment", comment.toString());

		const { data } = await apiPost<unknown>("/submit", formData);
		
		return data;
	});