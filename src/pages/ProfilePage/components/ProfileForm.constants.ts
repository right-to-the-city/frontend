export const baseThunkName = "profileForm";

export const getPrefix = (name: string) => {
	return `${baseThunkName}/${name}`;
};
