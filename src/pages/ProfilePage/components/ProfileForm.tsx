import { Input } from "@/components/shared/Input/Input";
import styles from "./ProfileForm.module.scss";
import { FocusEvent, useState } from "react";
import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { updateUserData } from "./ProfileForm.thunks";
import { User } from "@/types/user";
import { selectUser } from "@/stores/user/user.selectors";
import { setUser } from "@/stores/user/user.slice";

const isValidEmail = (email: string) => {
	const emailRegex = /^[a-zA-Z0-9_.%+-]+@[a-zA-Z0-9-]+\.[a-zA-Z]{2,}$/;
	return emailRegex.test(email);
};

export const ProfileForm = () => {
	const dispatch = useAppDispatch();
	const user = useAppSelector(selectUser);
  
	const [name, setName] = useState<string>(user?.name ?? "");
	const [username, setUsername] = useState<string>(user?.username ?? "");
	const [email, setEmail] = useState<string>(user?.email ?? "");

	if (!user) return null;

	const onBlurName = async (e: FocusEvent<HTMLInputElement>) => {
		if (!e.target.value) return;
		const { payload, meta } = await dispatch(updateUserData({ name: e.target.value, userId: user.id }));
		if (meta.requestStatus === "fulfilled") {
			dispatch(setUser(payload as User));
		}
	};
	const onBlurUsername = async (e: FocusEvent<HTMLInputElement>) => {
		if (!e.target.value) return;
		const { payload, meta } = await dispatch(updateUserData({ username: e.target.value, userId: user.id }));
		if (meta.requestStatus === "fulfilled") {      
			dispatch(setUser(payload as User));
		}
	};
	const onBlurEmail = async (e: FocusEvent<HTMLInputElement>) => {
		if (!e.target.value || !isValidEmail(e.target.value)) return;
		const { payload, meta } = await dispatch(updateUserData({ email: e.target.value, userId: user.id }));
		if (meta.requestStatus === "fulfilled") {
			dispatch(setUser(payload as User));
		}
	};

	return (
		<div>
			<div className={styles.title}>Личные данные</div>
			<div className={styles.fields}>
				<label>
					<span className={styles.label}>Имя</span>
					<Input onBlur={onBlurName} value={name} onChange={(e) => setName(e.target.value)} className={styles.input} placeholder="Введите имя"/>
				</label>
				<label>
					<span className={styles.label}>Логин</span>
					<Input onBlur={onBlurUsername} value={username} onChange={(e) => setUsername(e.target.value)} className={styles.input} placeholder="Введите логин" />
				</label>
				<label>
					<span className={styles.label}>Почта</span>
					<Input onBlur={onBlurEmail} type="email" value={email} onChange={(e) => setEmail(e.target.value)} className={styles.input} placeholder="Введите почту" />
				</label>
			</div>
		</div>
	);
};
