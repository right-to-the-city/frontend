
import { LEGACY_SMILE_BY_RATING } from "@/constants/rating";
import { RootState } from "@/store-utils/store";
import { createSelector } from "@reduxjs/toolkit";

const selectState = ({ analyticsRatingReducer }: RootState) => analyticsRatingReducer;

export const selectRating = createSelector(selectState, ({ rating }) => {
	return rating.map(({ count, rating }) => ({ type: LEGACY_SMILE_BY_RATING[rating], ["%"]: count }));
});

export const selectIsLoading = createSelector(selectState, ({ loading }) => {
	return loading;
});