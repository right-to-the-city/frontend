import { createSlice } from "@reduxjs/toolkit";
import { fetchAnalyticsRatingAsync } from "./AnalyticsRating.thunks";
import { baseThunkName } from "./AnalyticsRating.constants";
import { AnalyticsRating } from "@/types/analytics";

type AnalyticsRatingState = {
  rating: AnalyticsRating[];
  loading: boolean;
  error?: string;

}

const initialState: AnalyticsRatingState = { 
	loading: false,
	rating: [],
	error: undefined
};

const analyticsRatingSlice = createSlice({
	name: baseThunkName,
	initialState,
	reducers: {},
	extraReducers: ({ addCase }) => {
		addCase(fetchAnalyticsRatingAsync.pending, (state) => {
			state.loading = true;
			state.error = undefined;
		});

		addCase(fetchAnalyticsRatingAsync.fulfilled, (state, action) => {
			state.rating = action.payload;
			state.error = undefined;
			state.loading = false;
		});

		addCase(fetchAnalyticsRatingAsync.rejected, (state) => {
			state.error = "error";
			state.loading = false;
		});
	},
});

export const { 
	reducer: analyticsRatingReducer, 
} = analyticsRatingSlice;