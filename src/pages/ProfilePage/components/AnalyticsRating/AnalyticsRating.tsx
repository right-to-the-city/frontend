import { useEffect } from "react";
import { fetchAnalyticsRatingAsync } from "./AnalyticsRating.thunks";
import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { selectIsLoading, selectRating } from "./AnalyticsRating.selectors";
import { Bar, BarChart, CartesianGrid, ResponsiveContainer, Tooltip, XAxis, YAxis } from "recharts";
import { Spin } from "antd";

const AnalyticsRating = () => {
	const dispatch = useAppDispatch();
	const rating = useAppSelector(selectRating);
	const isLoading = useAppSelector(selectIsLoading);

	useEffect(() => {
		const thunk = dispatch(fetchAnalyticsRatingAsync());
		return () => {
			thunk.abort();
		};
	}, [dispatch]);

	if (isLoading) {
		return <Spin size="large" spinning />;
	}

	if (!rating.length) return null;

	return (
		<ResponsiveContainer width="100%" height="100%">
			<BarChart width={1000} height={600} data={rating}>
				<XAxis dataKey="type" />
				<YAxis />
				<Tooltip />
				<CartesianGrid strokeDasharray="5 5" />
				<Bar dataKey="%" fill="#1890FF" barSize={30} />
			</BarChart>
		</ResponsiveContainer>
		
	);
};

export default AnalyticsRating;