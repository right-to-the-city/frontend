import { apiGet } from "@/api/api";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { getPrefix } from "./AnalyticsRating.constants";
import { AnalyticsRating } from "@/types/analytics";

export const fetchAnalyticsRatingAsync = createAsyncThunk(
	getPrefix("rating"), 
	async () => {
		const { data } = await apiGet<AnalyticsRating[]>("/analytics/ratings");
		return data; 
	});
