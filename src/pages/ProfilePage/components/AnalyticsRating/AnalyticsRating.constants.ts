export const baseThunkName = "analyticsRating";

export const getPrefix = (name: string) => {
	return `${baseThunkName}/${name}`;
};
