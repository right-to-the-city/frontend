import { createSlice } from "@reduxjs/toolkit";
import { fetchAnalyticsGeographyAsync } from "./AnalyticsGeography.thunks";
import { baseThunkName } from "./AnalyticsGeography.constants";
import { AnalyticsGeography } from "@/types/analytics";

type AnalyticsGeographyState = {
  geography: AnalyticsGeography[];
  loading: boolean;
  error?: string;

}

const initialState: AnalyticsGeographyState = { 
	loading: false,
	geography: [],
	error: undefined
};

const analyticsGeographySlice = createSlice({
	name: baseThunkName,
	initialState,
	reducers: {},
	extraReducers: ({ addCase }) => {
		addCase(fetchAnalyticsGeographyAsync.pending, (state) => {
			state.loading = true;
			state.error = undefined;
		});

		addCase(fetchAnalyticsGeographyAsync.fulfilled, (state, action) => {
			const count = action.payload.otherAddresses.reduce<number>((acc, current) => {
				return acc + current.count;
			}, 0);

			state.geography = action.payload.topAddresses;
			if (count) {
				state.geography.push({ address: "Другие", count });
			}
			state.error = undefined;
			state.loading = false;
		});

		addCase(fetchAnalyticsGeographyAsync.rejected, (state) => {
			state.error = "error";
			state.loading = false;
		});
	},
});

export const { 
	reducer: analyticsGeographyReducer, 
} = analyticsGeographySlice;