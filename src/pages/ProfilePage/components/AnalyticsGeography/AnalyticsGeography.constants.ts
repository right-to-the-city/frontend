export const baseThunkName = "analyticsGeography";

export const getPrefix = (name: string) => {
	return `${baseThunkName}/${name}`;
};
