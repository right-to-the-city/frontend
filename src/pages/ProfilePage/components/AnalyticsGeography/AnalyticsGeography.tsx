import { useEffect, useState } from "react";
import { fetchAnalyticsGeographyAsync } from "./AnalyticsGeography.thunks";
import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { selectGeography, selectIsLoading } from "./AnalyticsGeography.selectors";
import { PieChart, Pie, Sector, ResponsiveContainer } from "recharts";
import { Spin } from "antd";

const renderActiveShape = (props: any) => {
	const RADIAN = Math.PI / 180;
	const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle, fill, payload, percent, address } = props;
	const sin = Math.sin(-RADIAN * midAngle);
	const cos = Math.cos(-RADIAN * midAngle);
	const sx = cx + (outerRadius + 10) * cos;
	const sy = cy + (outerRadius + 10) * sin;
	const mx = cx + (outerRadius + 30) * cos;
	const my = cy + (outerRadius + 30) * sin;
	const ex = mx + (cos >= 0 ? 1 : -1) * 22;
	const ey = my;
	const textAnchor = cos >= 0 ? "start" : "end";

	return (
		<g>
			<text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
				{payload.address}
			</text>
			<Sector
				cx={cx}
				cy={cy}
				innerRadius={innerRadius}
				outerRadius={outerRadius}
				startAngle={startAngle}
				endAngle={endAngle}
				fill={fill}
			/>
			<Sector
				cx={cx}
				cy={cy}
				startAngle={startAngle}
				endAngle={endAngle}
				innerRadius={outerRadius + 6}
				outerRadius={outerRadius + 10}
				fill={fill}
			/>
			<path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
			<circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
			<text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#1890FF">{`${address} (${(percent * 100).toFixed(2)}%)`}</text>
		</g>
	);
};

const AnalyticsGeography = () => {
	const dispatch = useAppDispatch();
	const geography = useAppSelector(selectGeography);
	const [activeIndex, setActiveIndex] = useState(0);
	const isLoading = useAppSelector(selectIsLoading);
	
	useEffect(() => {
		const thunk = dispatch(fetchAnalyticsGeographyAsync());
		return () => {
			thunk.abort();
		};
	}, [dispatch]);

	const onPieEnter = (_: any, index: any) => {
		setActiveIndex(index);
	};

	if (isLoading) {
		return <Spin size="large" spinning />;
	}
	if (!geography.length) return null;
	
	return (
		<ResponsiveContainer width="100%" height="100%">
			<PieChart width={400} height={400}>
				<Pie
					activeIndex={activeIndex}
					activeShape={renderActiveShape}
					data={geography}
					cx="50%"
					cy="50%"
					innerRadius={150}
					outerRadius={170}
					fill="#1890FF"
					dataKey="count"
					onMouseEnter={onPieEnter}
				/>
			</PieChart>
		</ResponsiveContainer>
	);
};

export default AnalyticsGeography;