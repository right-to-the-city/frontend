import { apiGet } from "@/api/api";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { getPrefix } from "./AnalyticsGeography.constants";
import { AnalyticsGeography } from "@/types/analytics";

export const fetchAnalyticsGeographyAsync = createAsyncThunk(
	getPrefix("addresses"), 
	async () => {
		const { data } = await apiGet<{topAddresses: AnalyticsGeography[]; otherAddresses: AnalyticsGeography[]}>("/analytics/addresses");
		return data; 
	});
