
import { RootState } from "@/store-utils/store";
import { createSelector } from "@reduxjs/toolkit";

const selectState = ({ analyticsGeographyReducer }: RootState) => analyticsGeographyReducer;

export const selectGeography = createSelector(selectState, ({ geography }) => {
	return geography.map(({ count, address }) => ({ address, count }));
});

export const selectIsLoading = createSelector(selectState, ({ loading }) => {
	return loading;
});