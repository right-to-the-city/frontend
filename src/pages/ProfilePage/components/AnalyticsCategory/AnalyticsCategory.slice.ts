import { createSlice } from "@reduxjs/toolkit";
import { fetchAnalyticsCategoryAsync } from "./AnalyticsCategory.thunks";
import { baseThunkName } from "./AnalyticsCategory.constants";
import { AnalyticsCategory } from "@/types/analytics";

type AnalyticsCategoryState = {
  rating: AnalyticsCategory[];
  loading: boolean;
  error?: string;

}

const initialState: AnalyticsCategoryState = { 
	loading: false,
	rating: [],
	error: undefined
};

const analyticsCategorySlice = createSlice({
	name: baseThunkName,
	initialState,
	reducers: {},
	extraReducers: ({ addCase }) => {
		addCase(fetchAnalyticsCategoryAsync.pending, (state) => {
			state.loading = true;
			state.error = undefined;
		});

		addCase(fetchAnalyticsCategoryAsync.fulfilled, (state, action) => {
			state.rating = action.payload;
			state.error = undefined;
			state.loading = false;
		});

		addCase(fetchAnalyticsCategoryAsync.rejected, (state) => {
			state.error = "error";
			state.loading = false;
		});
	},
});

export const { 
	reducer: analyticsCategoryReducer, 
} = analyticsCategorySlice;