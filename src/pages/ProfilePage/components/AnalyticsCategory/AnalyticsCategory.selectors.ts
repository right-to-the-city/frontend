
import { RootState } from "@/store-utils/store";
import { createSelector } from "@reduxjs/toolkit";

const selectState = ({ analyticsCategoryReducer }: RootState) => analyticsCategoryReducer;

export const selectCategory = createSelector(selectState, ({ rating }) => {
	return rating.map(({ count, category }) => ({ type: category, value: count }));
});

export const selectIsLoading = createSelector(selectState, ({ loading }) => {
	return loading;
});