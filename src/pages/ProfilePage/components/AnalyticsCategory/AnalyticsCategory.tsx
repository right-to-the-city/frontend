import { useEffect } from "react";
import { fetchAnalyticsCategoryAsync } from "./AnalyticsCategory.thunks";
import { useAppDispatch, useAppSelector } from "@/store-utils/hooks";
import { selectCategory, selectIsLoading } from "./AnalyticsCategory.selectors";
import { ResponsiveContainer, Tooltip, Treemap } from "recharts";
import { Spin } from "antd";

const AnalyticsCategory = () => {
	const dispatch = useAppDispatch();
	const categories = useAppSelector(selectCategory);
	const isLoading = useAppSelector(selectIsLoading);

	useEffect(() => {
		const thunk = dispatch(fetchAnalyticsCategoryAsync());
		return () => {
			thunk.abort();
		};
	}, [dispatch]);

	if (isLoading) {
		return <Spin size="large" spinning />;
	}

	if (!categories.length) return null;
	
	return (
		<ResponsiveContainer width="100%" height="100%">
			<Treemap
				width={400}
				height={200}
				data={categories}
				dataKey="value"
				aspectRatio={4 / 3}
				fill="#1890FF"
				stroke="#fff"
			>
				<Tooltip
					content={({ payload }) => {
						if (payload && payload[0]) {
							const data = payload[0].payload;
							return (
								<div style={{ background: "#fff", padding: "5px", border: "1px solid #ccc" }}>
									<p>{data.type}</p>
									<p>Доля: {data.value} %</p>
								</div>
							);
						}
						return null;
					}}
				/>
			</Treemap>
		</ResponsiveContainer>
	);
};

export default AnalyticsCategory;