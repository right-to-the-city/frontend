export const baseThunkName = "analyticsCategory";

export const getPrefix = (name: string) => {
	return `${baseThunkName}/${name}`;
};
