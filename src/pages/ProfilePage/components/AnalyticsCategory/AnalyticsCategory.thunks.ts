import { apiGet } from "@/api/api";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { getPrefix } from "./AnalyticsCategory.constants";
import { AnalyticsCategory } from "@/types/analytics";

export const fetchAnalyticsCategoryAsync = createAsyncThunk(
	getPrefix("rating"), 
	async () => {
		const { data } = await apiGet<AnalyticsCategory[]>("/analytics/categories");
		return data; 
	});
