import { apiPut } from "@/api/api";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { getPrefix } from "./ProfileForm.constants";
import { User } from "@/types/user";

type SubmitLoginFormAsyncParams = {
  username?: string;
  name?: string;
  email?: string;
	userId: number;
}

export const updateUserData = createAsyncThunk(
	getPrefix("login"), 
	async ({ username, name, email, userId }: SubmitLoginFormAsyncParams, { rejectWithValue }) => {
		try {
			const { data } = await apiPut<User>(`/user/${userId}`, { username, name, email });
			return data;
		} catch (error) {
			return rejectWithValue(error);
		}
	});