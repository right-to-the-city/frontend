
import styles from "./ProfilePage.module.scss";
import { ProfileForm } from "./components/ProfileForm";
import { Button } from "@/components/shared/Button/Button";
import { routePath } from "@/routes/routes.constants";
import { useAppSelector } from "@/store-utils/hooks";
import { selectUser } from "@/stores/user/user.selectors";
import AnalyticsRating from "./components/AnalyticsRating/AnalyticsRating";
import clsx from "clsx";
import AnalyticsCategory from "./components/AnalyticsCategory/AnalyticsCategory";
import AnalyticsGeography from "./components/AnalyticsGeography/AnalyticsGeography";

const ProfilePage = () => {
	const user = useAppSelector(selectUser);

	if (!user) return null;

	return (
		<div className={styles.container}>
			<div className={styles.leftSide}>
				<div className={styles.avatar}>
					{user.name?.[0].toUpperCase() ?? "A"}
				</div>
				<div className={styles.title}>
					{user.name}
				</div>
				<div className={styles.email}>
					{user.email}
				</div>
				<div className={styles.buttons}>
					<Button className={styles.button} href={routePath.my_objects}>Мои объекты</Button>
					<Button className={styles.button} href={routePath.my_grades}>Мои оценки</Button>
				</div>
			
			</div>

			<div className={styles.rightSide}>
				<ProfileForm />

				<h3 className={clsx(styles.title, styles.analyticTitle)}>Статистика</h3>
				<div className={styles.analyticCharts}>
					<div className={styles.chartContainer}>
						<h4 className={styles.subTitle}>Оценки</h4>
						<AnalyticsRating />
					</div>
					<div className={clsx(styles.chartContainer)}>
						<h4 className={styles.subTitle}>География</h4>
						<AnalyticsGeography />
					</div>
					<div className={styles.chartContainer}>
						<h4 className={styles.subTitle}>Категории</h4>
						<AnalyticsCategory />
					</div>
				</div>
			</div>
		</div>
	);
};

export default ProfilePage;