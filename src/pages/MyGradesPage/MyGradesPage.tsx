
import { PublicationList } from "./components/PublicationList/PublicationList";
import styles from "./MyGradesPage.module.scss";

const MyGradesPage = () => {

	return (
		<div className={styles.container}>
			<PublicationList />
		</div>
	);
};

export default MyGradesPage;