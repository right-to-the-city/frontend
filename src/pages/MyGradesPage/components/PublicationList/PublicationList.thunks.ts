import { apiGet } from "@/api/api";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { getPrefix } from "./PublicationList.constants";
import { MapObject } from "@/types/map";

type FetchObjectsAsyncParams = {
	userId?: number;
}

export const fetchObjectsAsync = createAsyncThunk(
	getPrefix("fetchObjects"), 
	async ({ userId }: FetchObjectsAsyncParams ) => {
		const { data } = await apiGet<{data: MapObject[]; total: number}>("/map", { params: { userId } });
		return data; 
	});
