import { RootState } from "@/store-utils/store";
import { createSelector } from "@reduxjs/toolkit";

const selectState = ({ myGradesReducer }: RootState) => myGradesReducer;

export const selectObjects = createSelector(selectState, ({ objects, search }) => {
	
	return objects.filter((object) => {
		const validValuesToSearch = Object.values(object).filter(Boolean);

		if (!search) {
			return true;
		}

		return JSON.stringify(validValuesToSearch).includes(search);
	});
});
export const selectIsLoading = createSelector(selectState, ({ loading }) => {
	return loading;
});
