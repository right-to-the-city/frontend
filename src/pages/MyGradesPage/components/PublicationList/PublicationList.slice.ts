import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { fetchObjectsAsync } from "./PublicationList.thunks";
import { baseThunkName } from "./PublicationList.constants";
import { MapObject } from "@/types/map";

type MapState = {
  objects: MapObject[];
  loading: boolean;
  error?: string;
	search: string;
}

const initialState: MapState = { 
	objects: [],
	loading: false,
	search: ""
};

const publicationsSlice = createSlice({
	name: baseThunkName,
	initialState,
	reducers: {
		resetState: () => initialState,
		setSearch: (state, action: PayloadAction<string>) => {
			state.search = action.payload;
		},
	},
	extraReducers: ({ addCase }) => {
		addCase(fetchObjectsAsync.pending, (state) => {			
			state.loading = true;
			state.error = undefined;
		});

		addCase(fetchObjectsAsync.fulfilled, (state, action) => {
			state.objects = action.payload.data;
			state.error = undefined;
			state.loading = false;
		});

		addCase(fetchObjectsAsync.rejected, (state) => {
			state.error = "error";
			state.loading = false;
		});
	},
});

export const { 
	reducer: publicationsReducer, 
	actions: { 
		resetState,
		setSearch
	}
} = publicationsSlice;