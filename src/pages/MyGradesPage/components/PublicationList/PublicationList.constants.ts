export const baseThunkName = "myGrades";

export const getPrefix = (name: string) => {
	return `${baseThunkName}/${name}`;
};
