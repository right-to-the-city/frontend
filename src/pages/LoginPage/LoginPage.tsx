import { Flex, FloatButton, Typography } from "antd";
import { LoginForm } from "./components/LoginForm/LoginForm";
import styles from "./LoginPage.module.scss";
import { Link } from "react-router-dom";
import { routePath } from "@/routes/routes.constants";
import { HeatMapOutlined } from "@ant-design/icons";

const LoginPage = () => {
	return (
		<Flex align="center" justify="center" vertical className={styles.container}>
			<Link to={routePath.map}><FloatButton tooltip={<div>Карта</div>} icon={<HeatMapOutlined />} /></Link>
			<Typography.Title className={styles.title} level={2}>Вход</Typography.Title>
			<LoginForm />
		</Flex>
	);
};

export default LoginPage;