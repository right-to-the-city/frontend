import { createSlice } from "@reduxjs/toolkit";
import { submitLoginFormAsync } from "./LoginForm.thunks";
import { baseThunkName } from "./LoginForm.constants";

type LoginState = {
  username: string;
  password: string;
  loading: boolean;
  error?: string;
}

const initialState: LoginState = { 
	username: "",
	password: "",
	loading: false 
};

const loginSlice = createSlice({
	name: baseThunkName,
	initialState,
	reducers: {},
	extraReducers: ({ addCase }) => {
		addCase(submitLoginFormAsync.pending, (state) => {
			state.loading = true;
			state.error = undefined;
		});

		addCase(submitLoginFormAsync.fulfilled, (state) => {
			state.error = undefined;
			state.loading = false;
		});

		addCase(submitLoginFormAsync.rejected, (state) => {
			state.error = "error";
		});
	},
});

export const { reducer: loginReducer } = loginSlice;