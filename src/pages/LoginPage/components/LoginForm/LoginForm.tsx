import { memo } from "react";
import { Button, Form, Input, message } from "antd";
import styles from "./LoginForm.module.scss";
import { submitLoginFormAsync } from "./LoginForm.thunks";
import { useAppDispatch } from "@/store-utils/hooks";
import { Link, useNavigate } from "react-router-dom";
import { routePath } from "@/routes/routes.constants";
import { AxiosError } from "axios";
import { setUser } from "@/stores/user/user.slice";
import { User } from "@/types/user";

export const LoginForm = memo(() => {
	const dispatch = useAppDispatch();

	const navigate = useNavigate();

	const [messageApi, contextHolder] = message.useMessage();

	const onSubmit = ({ password, login }: { password: string; login: string }) => {
		dispatch(submitLoginFormAsync({ login, password }))
			.then((response) => {				
				if (response.meta.requestStatus === "fulfilled") {
					dispatch(setUser(response.payload as User));
					navigate(routePath.map);
				}
				if (response.meta.requestStatus === "rejected") {
					const content = (response.payload as AxiosError<{error: string}>)?.response?.data?.error ?? "Ошибка входа";
					messageApi.error({ content });
				}
			});
	};
  
	return (
		<Form
			name="basic"
			className={styles.container}
			layout="vertical"
			initialValues={{ remember: true }}
			onFinish={onSubmit}
			autoComplete="off"
		>
			{contextHolder}
			<Form.Item
				label="Логин"
				name="login"
				rules={[{ required: true, message: "Это поле обязательное!" }]}
			>
				<Input />
			</Form.Item>

			<Form.Item
				label="Пароль"
				name="password"
				rules={[{ required: true, message: "Это поле обязательное!" }]}
			>
				<Input.Password />
			</Form.Item>

			<Form.Item className={styles.submitButtonContainer}>
				<Button type="primary" size="large" htmlType="submit">
					Войти
				</Button>

				<div>Или <Link to={routePath.registration}>Зарегистрироваться</Link></div>
			</Form.Item>
		</Form>
	);
});
