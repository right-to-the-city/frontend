export const baseThunkName = "loginForm";

export const getPrefix = (name: string) => {
	return `${baseThunkName}/${name}`;
};
