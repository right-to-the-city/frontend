import { apiPost } from "@/api/api";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { getPrefix } from "./LoginForm.constants";
import { User } from "@/types/user";

type SubmitLoginFormAsyncParams = {
  login: string;
  password: string;
}

export const submitLoginFormAsync = createAsyncThunk(
	getPrefix("login"), 
	async ({ login, password }: SubmitLoginFormAsyncParams, { rejectWithValue }) => {
		try {
			const { data } = await apiPost<User>("/login", { login, password });
			return data;
		} catch (error) {
			return rejectWithValue(error);
		}
	});