export const setLocalStorage = (key: string, value: any) => {
	try {
		localStorage.setItem(key, JSON.stringify(value));
	} catch {}
};

export const getLocalStorage = <T>(key: string, shouldParse = true) => {
	const value = localStorage.getItem(key);

	if (value) {
		return (shouldParse ? JSON.parse(value) : value) as T;
	}

	return value as null;
};

export const deleteFromLocalStorage = (key: string) => {
	localStorage.removeItem(key);
};
