export const debounce = (callback: (...ars: any) => void, time: number = 500) => {
	let timeoutId: any = null;

	return function (...args: any) {
		clearTimeout(timeoutId);
		timeoutId = setTimeout(() => {
			//@ts-ignore
			callback.apply(this, args);
		}, time);
	};
};