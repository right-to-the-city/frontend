
export const resizeXSScreenForMap = () => {
	if (window.screen.width > 576) {
		return;
	}

	setTimeout(() => {
		window.dispatchEvent(new Event("resize"));
	}, 300);
};