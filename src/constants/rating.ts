import { MapObject } from "@/types/map";

export const SMILE_BY_RATING: Record<number, string> = {
	"-5": "😡",
	"-4": "😩",
	"-3": "😖",
	"-2": "😣",
	"-1": "😒",
	"0": "😐",
	"1": "🙂",
	"2": "😊",
	"3": "😀",
	"4": "😁",
	"5": "😍",
};

export const LEGACY_SMILE_BY_RATING: Record<number, string> = {
	"0": "😡",
	"1": "😩",
	"2": "😖",
	"3": "😣",
	"4": "😒",
	"5": "😐",
	"6": "🙂",
	"7": "😊",
	"8": "😀",
	"9": "😁",
	"10": "😍",
};

export const getSmileByObject = (object: MapObject) => {
	const objectDate = new Date(object.date);
	const targetDate = new Date("2024-03-22");

	if (objectDate < targetDate || object.user.type === "tg") {
		return LEGACY_SMILE_BY_RATING[object.rating]; 
	}

	return SMILE_BY_RATING[object.rating];
};

export const getRatingByObject = (object: MapObject) => {
	const objectDate = new Date(object.date);
	const targetDate = new Date("2024-03-22");

	if (objectDate > targetDate && object.user.type === "web") {
		return object.rating; 
	}

	return object.rating - 5;
};