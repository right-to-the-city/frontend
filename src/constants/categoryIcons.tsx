import animals from "@/images/markers/animals.png";
import communication from "@/images/markers/communication.png";
import garbage from "@/images/markers/garbage.png";
import house from "@/images/markers/house.png";
import park from "@/images/markers/park.png";
import plants from "@/images/markers/plants.png";
import pub from "@/images/markers/pub.png";
import street from "@/images/markers/street.png";
import Pub from "@/images/categoriesIcons/pub.svg";
import Park from "@/images/categoriesIcons/park.svg";
import Animals from "@/images/categoriesIcons/animals.svg";
import Communication from "@/images/categoriesIcons/communication.svg";
import Garbage from "@/images/categoriesIcons/garbage.svg";
import House from "@/images/categoriesIcons/house.svg";
import Plants from "@/images/categoriesIcons/plants.svg";
import Street from "@/images/categoriesIcons/street.svg";

export const ICON_BY_CATEGORIES = {
	"Здание | Дом": house,
	"Улица": street,
	"Двор | Парк": park,
	"Коммуникации": communication,
	"Животные": animals,
	"Природа | Растения": plants,
	"Заведения": pub,
	"Непорядок": garbage
};
export const ICON_COMPONENTS_BY_CATEGORIES = {
	"Здание | Дом": <House />,
	"Улица": <Street />,
	"Двор | Парк": <Park />,
	"Коммуникации": <Communication />,
	"Животные": <Animals />,
	"Природа | Растения": <Plants />,
	"Заведения": <Pub />,
	"Непорядок": <Garbage />
};