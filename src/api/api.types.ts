import type { AxiosRequestConfig, AxiosResponse } from "axios";

export type RequestWithoutDataType = <Response = unknown, Data = unknown, Params = unknown>(
    url: string,
    config?: Omit<AxiosRequestConfig<Data>, "params"> & { params?: Params }
) => Promise<AxiosResponse<Response, Data>>;

export type RequestWithDataType = <Response = unknown, Data = unknown, Params = unknown>(
    url: string,
    data?: Data,
    config?: Omit<AxiosRequestConfig<Data>, "params"> & { params?: Params }
) => Promise<AxiosResponse<Response, Data>>;