import ErrorBoundary from "antd/es/alert/ErrorBoundary";
import { Navigate, createBrowserRouter, redirect } from "react-router-dom";
import { routePath } from "./routes.constants";
import AppLayout from "@/components/pageComponents/AppLayout";
import { Suspense, lazy } from "react";
import { RoleRoute } from "./components/RoleRoute";

export const LoginPage = lazy(
	async () =>
		await import(
			"@/pages/LoginPage"
		)
);

export const RegistrationPage = lazy(
	async () =>
		await import(
			"@/pages/RegistrationPage"
		)
);

export const MapPage = lazy(
	async () =>
		await import(
			"@/pages/MapPage/MapPage"
		)
);

export const HeatMapPage = lazy(
	async () =>
		await import(
			"@/pages/MapPage/HeatMapPage"
		)
);
export const ProfilePage = lazy(
	async () =>
		await import(
			"@/pages/ProfilePage"
		)
);
export const PublicationsPage = lazy(
	async () =>
		await import(
			"@/pages/PublicationsPage"
		)
);
export const UploadPage = lazy(
	async () =>
		await import(
			"@/pages/UploadPage"
		)
);
export const AdminPanelPage = lazy(
	async () =>
		await import(
			"@/pages/AdminPanelPage"
		)
);
export const MyObjectsPage = lazy(
	async () =>
		await import(
			"@/pages/MyObjectsPage"
		)
);
export const MyGradesPage = lazy(
	async () =>
		await import(
			"@/pages/MyGradesPage"
		)
);

export const appRouter = createBrowserRouter([
	{
		path: routePath.login,
		element: (
			<Suspense>
				<LoginPage />
			</Suspense>
		),
	},
	{
		path: routePath.registration,
		element: (
			<Suspense>
				<RegistrationPage />
			</Suspense>
		),
	},
	{
		path: "/",
		element: (
			<AppLayout />
		),
		ErrorBoundary: ErrorBoundary,
		children: [
			{
				index: true,
				loader: async () => redirect(routePath.map)
			},
			{
				path: routePath.map,
				element: (
					<Suspense>
						<MapPage />
					</Suspense>
				),
			},
			{
				path: routePath.heatMap,
				element: (
					<Suspense>
						<HeatMapPage />
					</Suspense>
				),
			},
			{
				path: routePath.upload,
				element: (
					<Suspense>
						<UploadPage />
					</Suspense>
				),
			},
			{
				path: routePath.my_objects,
				element: (
					<Suspense>
						<MyObjectsPage />
					</Suspense>
				)
			},
			{
				path: routePath.my_grades,
				element: (
					<Suspense>
						<MyGradesPage />
					</Suspense>
				)
			},
			{
				path: routePath.profile,
				element: (
					<Suspense>
						<ProfilePage />
					</Suspense>
				),
			},
			{
				path: routePath.publications,
				element: (
					<Suspense>
						<PublicationsPage />
					</Suspense>
				),
			},
			{
				path: routePath.adminPanel,
				element: (
					<Suspense>
						<RoleRoute>
							<AdminPanelPage />
						</RoleRoute>
					</Suspense>
				),
			}
		],
	},

	// last->
	{
		path: routePath.not_found,
		element: <Navigate to={routePath.map} replace />,
	},
]);
