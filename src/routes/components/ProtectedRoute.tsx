import { FC, ReactNode } from "react";
import { Navigate } from "react-router-dom";
import { routePath } from "../routes.constants";
import { useAppSelector } from "@/store-utils/hooks";
import { selectUser } from "@/stores/user/user.selectors";

type ProtectedRouteProps = {
  children: ReactNode;
}

export const ProtectedRoute: FC<ProtectedRouteProps> = ({ children }) => {
	const user = useAppSelector(selectUser);

	const isAuth = !!user;
  
	if (!isAuth) {
		return <Navigate to={routePath.map} />;
	}

	return children;
};