import { FC, ReactNode } from "react";
import { Navigate } from "react-router-dom";
import { routePath } from "../routes.constants";
import { useAppSelector } from "@/store-utils/hooks";
import { selectUser } from "@/stores/user/user.selectors";

type RoleRouteProps = {
  children: ReactNode;
}

export const RoleRoute: FC<RoleRouteProps> = ({ children }) => {
	const user = useAppSelector(selectUser);
  
	if (!user || user.priority !== 1) {
		return <Navigate to={routePath.map} replace />;
	}

	return children;
};