export enum AppRoutes {
  LOGIN = "login",
  MAP = "map",
  HEAT_MAP = "heatMap",
  UPLOAD = "upload",
  REGISTRATION = "registration",
  PROFILE = "profile",
  MY_OBJECTS="my_objects",
  MY_GRADES="my_grades",
  PUBLICATIONS = "publications",
  ANALYTICS = "analytics",
  ADMIN_PANEL = "adminPanel",

  // last->
  NOT_FOUND = "not_found",
}

export const routePath: Record<AppRoutes, string> = {
	[AppRoutes.LOGIN]: "/login",
	[AppRoutes.MAP]: "/map",
	[AppRoutes.HEAT_MAP]: "/heatMap",
	[AppRoutes.UPLOAD]: "/upload",
	[AppRoutes.REGISTRATION]: "/registration",
	[AppRoutes.PROFILE]: "/profile",
	[AppRoutes.MY_OBJECTS]: "/profile/my-objects",
	[AppRoutes.MY_GRADES]: "/profile/my-grades",
	[AppRoutes.PUBLICATIONS]: "/publications",
	[AppRoutes.ANALYTICS]: "/analytics",
	[AppRoutes.ADMIN_PANEL]: "/adminPanel",

	// last->
	[AppRoutes.NOT_FOUND]: "*",
};
