module.exports = {
	root: true,
	env: { browser: true, es2021: true },
	extends: [
		"plugin:react/recommended",
		"eslint:recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:react-hooks/recommended",
	],
	ignorePatterns: ["dist", ".eslintrc.cjs"],
	parser: "@typescript-eslint/parser",
	plugins: ["react", "react-refresh"],
	rules: {
		semi: 2,
		quotes: ["error", "double"],
		"object-curly-spacing": ["error", "always"],
		indent: ["error", "tab"],
		"no-empty": "off",
		"no-multi-spaces": "error",
		"no-multiple-empty-lines": ["error", { "max": 1, "maxEOF": 0 }],
		"space-infix-ops": "error",

		"react/display-name": "off",
		"react/jsx-indent": ["error", "tab"],
    "react/jsx-indent-props": ["error", "tab"],
    "react/react-in-jsx-scope": "off",
		"react-hooks/exhaustive-deps": "off",
		"react/prop-types": "off",

		"@typescript-eslint/ban-ts-comment": "off",
		"@typescript-eslint/no-explicit-any": "off",
		"@typescript-eslint/member-delimiter-style": ["error", {
      multiline: {
        delimiter: 'semi',
        requireLast: true,
      },
      singleline: {
        delimiter: 'semi', 
        requireLast: false,
      },
    }],
		"@typescript-eslint/type-annotation-spacing": "error"
	},
};