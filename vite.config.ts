import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import path from "path";
import svgr from "vite-plugin-svgr";

export default defineConfig({
	plugins: [react(), svgr({ include: "**/*.svg" }),],
	envPrefix: "APP_",
	resolve: {
		alias: {
			"@": path.resolve(__dirname, "./src"),
		},
	},
});
